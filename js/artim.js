/* This class switches x and y from stack and uses z y x */

// Add a clearing function

(function() {
	
function ARTimage(){} function artim() {
	var out = new ARTimage();
	out.init();
	return out;
}

ARTimage.prototype = {
	filetypes: {
		"jsi": function(){return new JSI();},
		"zvi": function(){return new ZVI();},
		// Add new filetypes by adding the extension and the appropriate File Reader object, conforming to the following standards:
	},
	
	load: function(files) {
		this.clear();
		
		this.pars.files = [];
		this.pars.file = 0;
		this.pars.z = 0;
		this.justloaded = true;
		
		for (var i = 0; i < files.length; i++) {
			var ext = this.extension(files[i].name);
			if (ext) {this.pars.files.push([files[i]]);}
		}
		this.pars.files.sort(this.sort); // ADDED!! Was this.pars.files.sort().sort(this.sortAscending0);
//		this.pars.files.sort().sort(this.sortAscending0);
		
		if (this.pars.files.length > 0) {this.open(0);}
		else {return false;}
	},
	
	open: function(file) {
		this.clear();
		
		var open = 0;
		if (typeof file == "number") {
			if (file >= 0 && file < this.pars.files.length) {open = file;}
		}
		else if (typeof file == "string") {
			var found = -1, i = 0;
			while (i < this.pars.files.length && found == -1) {
				if (this.pars.files[i][0].name == file) {found = i;}
			}
			if (found > -1) {open = found;}
		}
		
		var self = this;
		this.pars.file = open;
		this.pars.extension = this.extension(this.pars.files[this.pars.file][0].name);
		
		var reader = new FileReader();
		reader.onload = function(e) {self.read(e.target.result)};
//	REMOVE THIS!!! -----------------------------------------------------
		if (this.pars.extension == "jsi") {reader.readAsText(this.pars.files[this.pars.file][0]);}
//	--------------------------------------------------------------------
		else {reader.readAsBinaryString(this.pars.files[this.pars.file][0]);}
		return this;
	},
	
	z: function(set) {
		if (set === undefined) {return this.pars.z;}
		else {
			this.pars.z = this.bound(set, [0, this.pars.zs - 1]);
			
			if (this.pars.pos) {
				this.pars.pos[2] = this.pars.z;
				this.callFn(this.moveFn, this.pars.pos);
			}
			this.update();
			return this;
		}
	},
	midz: function() {
		this.pars.z = Math.floor(this.pars.zs/2.);
		this.update();
	},
	
	up: function() {this.z(this.pars.z - 1); return this;},
	dn: function() {this.z(this.pars.z + 1); return this;},
	top: function() {this.z(0); return this;},
	bot: function() {this.z(this.pars.zs - 1); return this;},
	
	fw: function() {this.open(this.pars.file + 1); return this;},
	bk: function() {this.open(this.pars.file - 1); return this;},
	beg: function() {this.open(0); return this;},
	end: function() {this.open(this.pars.files.length - 1); return this;},
	
	zoom: function(inorout) {
		if (inorout == 'in') {this.pars.zoom = this.bound(this.pars.zoom + 1, [1, 30]);}
		else if (inorout == "out") {this.pars.zoom = this.bound(this.pars.zoom - 1, [1, 30]);}
		else if (typeof inorout == "number") {this.pars.zoom = this.bound(Math.round(inorout), [1, 30]);}
		this.size();
		this.update();
		this.callFn(this.moveFn, this.pars.pos);
	},
	id: function() {return [this.pars.reader.id(), this.pars.files[this.pars.file][0].name];},
	
	mode: function() {
		if (this.pars.gl === true) {
			if (this.pars.fshadertype === 'exp') {this.gl();}
			else {this.gl('exp');}
		}
		return this;
	},
	reset: function() {this.pars.mmoffset = [0, 0]; this.update(); return this;},
	flatten: function(zmin, zmax) {
		if (zmin === undefined) {zmin = 0;}
		if (zmax === undefined) {zmax = this.pars.zs;}
		if (this.data.length == this.pars.zs) {
			var out = [], max;
			for (var y = 0; y < this.pars.height; y++) {
				out.push([]);
				for (var x = 0; x < this.pars.width; x++) {
					for (var z = zmin; z < zmax; z++) {
						if (z == zmin) {max = this.data[z][y][x];}
						else if (this.data[z][y][x] > max) {max = this.data[z][y][x];}
					}
					out[y].push(max);
				}
			}
			console.log(out.length, out[0].length);
			this.data.push(out);
			
			for (var mmin, mmax, z = zmin; z < zmax; z++) {
				if (z == 0) {mmin = this.pars.mnmx[z][0]; mmax = this.pars.mnmx[z][1];}
				else {
					if (this.pars.mnmx[z][0] < mmin) {mmin = this.pars.mnmx[z][0];}
					if (this.pars.mnmx[z][1] > mmax) {mmax = this.pars.mnmx[z][1];}
				}
			}
			
			this.pars.minmaxed.push(true);
			this.pars.mnmx.push([mmin, mmax, 255./(mmax - mmin)]);
		}
		else {
			var max;
			for (var y = 0; y < this.pars.height; y++) {
				for (var x = 0; x < this.pars.width; x++) {
					for (var z = 0; z < this.pars.zs; z++) {
						if (z == 0) {max = this.data[z][y][x];}
						else if (this.data[z][y][x] > max) {max = this.data[z][y][x];}
					}
					this.data[this.pars.zs][y][x] = max;
				}
			}
			
			for (var z = 0; z < this.pars.zs; z++) {
				if (z == 0) {this.pars.mnmx[this.pars.zs][0] = this.pars.mnmx[z][0]; this.pars.mnmx[this.pars.zs][1] = this.pars.mnmx[z][1];}
				else {
					if (this.pars.mnmx[z][0] < this.pars.mnmx[this.pars.zs][0]) {this.pars.mnmx[this.pars.zs][0] = this.pars.mnmx[z][0];}
					if (this.pars.mnmx[z][1] > this.pars.mnmx[this.pars.zs][1]) {this.pars.mnmx[this.pars.zs][1] = this.pars.mnmx[z][1];}
				}
			}
			this.pars.mnmx[this.pars.zs][2] = 255./(this.pars.mnmx[this.pars.zs][1] - this.pars.mnmx[this.pars.zs][0]);
		}
		
		this.pars.z = this.pars.zs;
		this.update();
		return this;
	},
	mm: function(min, max) {
		if (min === undefined || max === undefined) {this.minmax(this.pars.z, true);}
		else if (Math.floor(min) <= Math.floor(max)) {this.pars.mnmx[this.pars.z] = [Math.floor(min), Math.floor(max)];}
		else {this.pars.mnmx[this.pars.z] = [Math.floor(max), Math.floor(min)];}
		this.update();
		return this;
	},
	download: function(el, callback) {
		function pad(zin, zmax) {
			ln = Math.floor(Math.log(zmax)/Math.LN10);
			zin = zin.toString();
			return zin.length > ln ? zin : new Array(ln - zin.length + 1).join('0') + zin;
		}
		var can = document.createElement('canvas');
		var ctx = can.getContext('2d');
		can.setAttribute('width', this.pars.width);
		can.setAttribute('height', this.pars.height);
		var ran = this.pars.mnmx[this.pars.z][1] - this.pars.mnmx[this.pars.z][0];
		var bds = [ran*(1 - this.pars.mmoffset[0]), this.pars.mnmx[this.pars.z][0] + ran*this.pars.mmoffset[1]];
		for (y = 0; y < this.pars.height; y++) {
			for (x = 0; x < this.pars.width; x++) {
				var p = (x + y*this.pars.width)*4;
				var v = (this.data[this.pars.z][y][x] - bds[1])/bds[0];
				if (this.pars.fshadertype == 'exp') {v = v*v;}
				v = Math.floor(v*255);
				ctx.fillStyle = "rgb(" + v + "," + v + "," + v + ")";
				ctx.fillRect(x, y, 1, 1);
			}
		}
		if (typeof el === 'string') {el = document.getElementById(el);}
		var title = this.pars.files[this.pars.file][0].name.replace('.'+this.extension(this.pars.files[this.pars.file][0].name), '');
		var ztext = this.pars.z < this.pars.zs ? '-z'+pad(this.pars.z, this.pars.zs) : '-zFlat';
		var dtext = this.pars.fshadertype == 'exp' ? '-dExp' : '-dLin';
		el.innerHTML = '<a id="__temp_download_link" href="'+can.toDataURL('png')+'" download="'+title+ztext+dtext+'.png">DOWNLOAD</a>';
		if (callback !== undefined) {document.getElementById('__temp_download_link').onmouseup = callback;}
		return this;
	},
	
	onmove: function(fn) {this.moveFn.push(fn); return this;},
	onover: function(fn) {this.overFn.push(fn); return this;},
	onclick: function(fn) {this.clickFn.push(fn); return this;},
	onkey: function(key, fn) {
		var codes = {'backspace':8, 'tab':9, 'enter':13, 'shift':16, 'ctrl':17, 'alt':18, 'option':18, 'escape':27, 'left arrow':37, 'up arrow': 38, 'right arrow':39, 'down arrow':40, '0':48, '1':49, '2':50, '3':51, '4':52, '5':53, '6':54, '7':55, '8':56, '9':57, 'a':65, 'b':66, 'c':67, 'd':68, 'e':69, 'f':70, 'g':71, 'h':72, 'i':73, 'j':74, 'k':75, 'l':76, 'm':77, 'n':78, 'o':79, 'p':80, 'q':81, 'r':82, 's':83, 't':84, 'u':85, 'v':86, 'w':87, 'x':88, 'y':89, 'z':90, 'multiply':106, '*':106, 'add':107, 'plus':107, '+':107, 'subtract':189, 'minus':189, '-':189, 'semicolon':186, 'equals':187, '=':187, 'comma':188, ',':188, 'dash':189, 'period':190, '.':190, 'forward slash':191, '/':191, 'open bracket':219, '[':219, 'back slash':220, '\\':220, 'close bracket':221, ']':221, 'quote':222, "'":222};
		if (typeof key == "number") {this.keyFn[key] = fn;}
		else {this.keyFn[codes[key]] = fn;}
		return this;
	},
	onupdate: function(fn) {this.updateFn.push(fn); return this;},
	onopen: function(fn) {this.openFn.push(fn); return this;},
	onready: function(fn) {this.loadFn.push(fn); return this;},
	onprogress: function(fn) {this.progressFn.push(fn); return this;},
	onfullyloaded: function(fn) {this.fullLoadFn.push(fn); return this;},
	onresize: function(fn) {
		this.resizeFn.push(fn);
		var self = this;
		try {window.addEventListener("resize", function(){self.callFn(self.resizeFn);});}
		catch (e) {window.attachEvent("onresize", function(){self.callFn(self.resizeFn);});}
		return this;
	},
	elclick: function(el, fn) {
		if (typeof el === "string") {el = document.getElementById(el);}
		el.onclick = fn;
		return this;
	},
	elcontents: function(el, val) {
		if (el !== undefined) {
			if (typeof el === "string") {el = document.getElementById(el);}
			if (el.tagName.toLowerCase() === "input" || el.tagName.toLowerCase() === "select" || el.tagName.toLowerCase() === "textarea") {el.value = val;}
			else {el.innerHTML = val;}
		}
		return this;
	},
	
	addcanvas: function(divID) {
		this.pars.containerdiv = document.getElementById(divID);
		this.pars.containerdiv.innerHTML = '<canvas id="_artim_im" style="position:absolute;top:0px;left:0px;z-index:0;"></canvas><canvas id="_artim_mk" style="position:absolute;top:0px;left:0px;z-index:1;">Your browser does not support HTML5. Please use the Chrome or an updated version of Firefox.</canvas>';
		this.pars.imagediv = document.getElementById('_artim_im');
		this.pars.markdiv = document.getElementById('_artim_mk');
//		this.ctx = this.pars.imagediv.getContext('2d'); MOVED FOR GL. DELETE IF GL WORKS
		this.mctx = this.pars.markdiv.getContext('2d');
		this.containerbinding();
		return this;
	},
	setcontainer: function(divID) {this.pars.containerdiv = document.getElementById(divID); return this;},
	setmark: function(markID) {
		this.pars.markdiv = document.getElementById(markID);
		this.mctx = this.pars.markdiv.getContext('2d');
		return this;
	},
	setinterface: function(o) { // o is an object of ids
		// o.x = id of x element, o.y, o.z, o.intensity, o.title
		var self = this;
		var pairs = {
			'x':['move', function(el) {if (self.pars.pos != null) {self.elcontents(el, self.pars.pos[0]);}}],
			'y':['move', function(el) {if (self.pars.pos != null) {self.elcontents(el, self.pars.pos[1]);}}],
			'intensity':['move', function(el) {if (self.pars.pos != null) {self.elcontents(el, self.get());}}],
			'z':['update', function(el) {self.elcontents(el, self.pars.z);}],
			'min':['update', function(el) {if (self.pars.mnmx !== undefined && self.pars.mnmx[self.pars.z] !== undefined) {self.elcontents(el, self.pars.mnmx[self.pars.z][0]);}}],
			'max':['update', function(el) {if (self.pars.mnmx !== undefined && self.pars.mnmx[self.pars.z] !== undefined) {self.elcontents(el, self.pars.mnmx[self.pars.z][1]);}}],
			'title':['open', function(el) {self.elcontents(el, self.pars.files[self.pars.file][0].name);}],
		};
		
		for (key in o) {if (o.hasOwnProperty(key)) {
			if (key in pairs) {
				(function(self, pair, el) {
					self['on' + pair[0]](function() {pair[1](el);});
				})(this, pairs[key], o[key]);
			}
		}}
		
		return this;
	},
	
	get: function(p) {
		if (p === undefined || p.length == 0) {p = [this.pars.pos[0], this.pars.pos[1], this.pars.z];}
		else if (p.length == 2) {p.push(this.pars.z);}
		if (p.length == 1 || p.length > 3) {return false;}
		
		p = [Math.round(p[0]), Math.round(p[1]), p[2]];
		this.loadz(p[2]);
		return (p[0] >= 0 && p[1] >= 0 && p[2] >= 0 && p[0] < this.pars.width && p[1] < this.pars.height && p[2] < this.data.length) ? this.data[p[2]][p[1]][p[0]] : 0;
	},
	
	cube: function(p, d) {
		var out = [];
		for (var z = 0; z < d[2]; z++) {
			out.push([]);
			for (var y = 0; y < d[1]; y++) {
				out[z].push([]);
				for (var x = 0; x < d[0]; x++) {
					out[z][y].push(this.data[p[2]+z][p[1]+y][p[0]+x]);
				}
			}
		}
		return out;
	},
	
	cylinder: function(p, r) { // p[x,y,z], radius
		function lsort(a, b) {return (a[0] - b[0]);}
		rint = Math.ceil(r);
		var out = [];
		for (x = -rint; x <= rint; x++) {
			for (y = -rint; y <= rint; y++) {
				d = Math.sqrt(x*x + y*y);
				if (d < r) {out.push([d, this.data[p[2]][p[1]+y][p[0]+x]]);}
			}
		}
		return out.sort(lsort);
	},
	
	sum: function(p, r, up, dn, ann) { // Sums in circles, if ann is set, it background subtracts the surrounding annulus
		if (up === undefined) {up = 0;}
		if (dn === undefined) {dn = up;}
		
		if (p === undefined || p.length == 0) {p = [this.pars.pos[0], this.pars.pos[1], this.pars.z];}
		else if (p.length == 2) {p.push(this.pars.z);}
		
		if (r === undefined) {return false;}
		
		var x = Math.round(p[0]); var y = Math.round(p[1]); var z = p[2];
		var rr = r*r;
		
		if (ann === undefined || ann <= 0) {
			r = Math.ceil(r);
			
			if (x - r >= 0 && x + r < this.pars.width && y - r >= 0 && y + r < this.pars.height && z - dn >= 0 && z + up < this.pars.zs) {
				var s = 0;
				for (var zo = -1*dn; zo <= up; zo++) {
					this.loadz(z+zo);
					for (var yo = -1*r; yo <= r; yo++) {
						for (var xo = -1*r; xo <= r; xo++) {
							if (yo*yo + xo*xo < rr) {
								s += this.data[z+zo][y+yo][x+xo];
							}
						}
					}
				}
				return s;
			}
			else {return false;}
		}
		else {
			var rara = (r + ann)*(r + ann);
			r = Math.ceil(r);
			ann = Math.ceil(ann);
			
			if (x - r - ann >= 0 && x + r + ann < this.pars.width && y - r - ann >= 0 && y + r + ann < this.pars.height && z - dn >= 0 && z + up < this.pars.zs) {
				var s = 0;
				for (var zo = -1*dn; zo <= up; zo++) {
					this.loadz(z+zo);
					var bg = 0;
					var bgn = 0;
					var sz = 0;
					var szn = 0;
					
					for (var yo = -1*(r + ann); yo <= (r + ann); yo++) {
						for (var xo = -1*(r + ann); xo <= (r + ann); xo++) {
							if (yo*yo + xo*xo < rr) {
								sz += this.data[z+zo][y+yo][x+xo];
								szn++;
							}
							else if (yo*yo + xo*xo < rara) {
								bg += this.data[z+zo][y+yo][x+xo];
								bgn++;
							}
						}
					}
					
					s += sz - szn*(bg/bgn);
				}
				
				return s;
			}
			else {return false;}
		}
	},
	
	perspective: function(p, d, w, ctx) {
		var c = [4,2,12]; // camera: x,y,z
		var e = [4,2,16]; // viewer's position
		var vt = [7.68, 4.48]; // view translation
		var vs = [w/2.0591304347826096, w/2.0591304347826096]; // view scale
		
		function transform(a) {
			var d = [
				a[0] - c[0],
				a[1] - c[1],
				a[2] - c[2],
			];
			var b = [
				(d[0] - e[0])*(e[2]/d[2]),
				(d[1] - e[1])*(e[2]/d[2]),
			];
			var v = [
				(b[0] - vt[0])*vs[0],
				(b[1] - vt[1])*vs[1],
			];
			return v;
		}
		
		function draw(np, nd, clr) {
			ctx.fillStyle = clr;
			ctx.beginPath();
			var t = transform(np); ctx.moveTo(t[0], t[1]);
			np[0] += nd[0]; t = transform(np); ctx.lineTo(t[0], t[1]);
			np[2] += nd[2]; t = transform(np); ctx.lineTo(t[0], t[1]);
			np[0] -= nd[0]; t = transform(np); ctx.lineTo(t[0], t[1]);
			np[2] -= nd[2]; t = transform(np); ctx.lineTo(t[0], t[1]);
			ctx.closePath();
			ctx.fill();
			
		}
		
		var dat = this.cube(p, d);
		var proto = [[1, 0.5, -0.5], [1 ,-0.5, 1]];
		var px = proto[1][0]/d[0];
		var pxd = [px, proto[1][1], px];
		
		ctx.clearRect(0, 0, w, w*(d[1] + 1));
		
		for (var z = 0; z < d[2]; z++) {
			for (var x = 0; x < d[0]; x++) {
				for (var y = 0; y < d[1]; y++) {
					var pos = [proto[0][0] + (d[0] - 1 - x)*px, proto[0][1] + (d[2] - 1 - z)*proto[1][1], proto[0][2] + y*px];
					var clr = Math.round((dat[z][y][x] - this.pars.mnmx[p[2] + z][0])*this.pars.mnmx[p[2] + z][2]);
					clr = 'rgb(' + clr + ',' + clr + ',' + clr + ')';
					draw(pos, pxd, clr);
				}
			}
		}
	},
	
	mult: function() {
		if (this.pars.displayMult) {this.pars.displayMult = false;}
		else {this.pars.displayMult = true;}
		this.update();
		return this;
	},
	
	width: function() {return this.pars.width;},
	height: function() {return this.pars.height;},
	files: function() {return this.pars.files;},
	file: function() {return this.pars.file;},
	
	parameters: function() {
		return {
			'width':this.pars.width,
			'height':this.pars.height,
			'files':this.pars.files,
			'file':this.pars.file,
		};
	},
	
//  ----------------------------------------------------------------------
	drawpoint: function(p, clr) {
		this.mctx.fillStyle = clr;
		this.mctx.fillRect(p[0]*this.pars.zoom, p[1]*this.pars.zoom, this.pars.zoom, this.pars.zoom);
		return this;
	},
	
	drawcircle: function(p, r, w, clr) {
		if (clr === undefined) {clr = w; w = false;}
		this.mctx.strokeStyle = clr;
		this.mctx.beginPath();
		if (w !== undefined && w != false) {this.mctx.lineWidth = w*this.pars.zoom;}
		this.mctx.arc(p[0]*this.pars.zoom, p[1]*this.pars.zoom, r*this.pars.zoom, 0, Math.PI*2, true);
		this.mctx.closePath();
		this.mctx.stroke();
		this.mctx.lineWidth = 1;
		return this;
	},
	
	drawpixel: function(p, clr) {
		this.mctx.strokeStyle = clr;
		this.mctx.strokeRect(p[0]*this.pars.zoom, p[1]*this.pars.zoom, this.pars.zoom, this.pars.zoom);
		return this;
	},
	
	drawpath: function(path, clr) {
		this.mctx.strokeStyle = clr;
		this.mctx.beginPath();
		this.mctx.lineWidth = 1*this.pars.zoom;
		this.mctx.moveTo(path[0][0]*this.pars.zoom, path[0][1]*this.pars.zoom);
		for (var i = 1; i < path.length; i++) {this.mctx.lineTo(path[i][0]*this.pars.zoom, path[i][1]*this.pars.zoom);}
		this.mctx.stroke();
	},
	
	drawpolygon: function(path, clr) {
		this.mctx.strokeStyle = clr;
		this.mctx.beginPath();
		this.mctx.lineWidth = 1*this.pars.zoom;
		this.mctx.moveTo(path[0][0]*this.pars.zoom, path[0][1]*this.pars.zoom);
		for (var i = 1; i < path.length; i++) {this.mctx.lineTo(path[i][0]*this.pars.zoom, path[i][1]*this.pars.zoom);}
		this.mctx.closePath();
		this.mctx.stroke();
	},
	
	cleardraw: function() {
		this.mctx.clearRect(0, 0, this.pars.width*this.pars.zoom, this.pars.height*this.pars.zoom);
		return this;
	},
	
	drawposition: function() {
		im.onmove(function(p){
			im.cleardraw().drawpoint(p, 'rgba(255,0,0, 0.8)');
		});
	},
		
//  ----------------------------------------------------------------------
//	BEHIND THE SCENES

	pars: {
		width: 0,
		height: 0,
		z: 0,
		zs: 1,
		bits: 8,
		async: false,
		file: 0,
		files: [],
		buffered: [],
		zoom: 2,
	},
	
	init: function() {
		this.time = {
			framerate: 28,
			millis: 36,
			last: +new Date(),
		};
		this.clear();
		
		this.pars.containerdiv = false;
		this.pars.imagediv = false;
		this.pars.markdiv = false;
		this.pars.gl = false;
		this.pars.mmoffset = [0, 0];
		this.ctx = false;
		this.mctx = false;
		
		this.hasfile = false;
		
		this.moveFn = [];
		this.overFn = [];
		this.clickFn = [];
		this.keyFn = {};
		this.updateFn = [];
		this.openFn = [];
		this.loadFn = [];
		this.progressFn = [];
		this.fullLoadFn = [];
		this.resizeFn = [];
		
		this.docbinding();
		var self = this;
		this.onresize(function(){self.offset(true);});
	},
	
	go: function() {return this.hasfile;},

	callFn: function(fn, arg, arg2) {
		if (this.go()) {
			for (var i = 0; i < fn.length; i++) {fn[i](arg, arg2);}
		}
		return this;
	},

	read: function(data) {
		var imageReader = this.filetypes[this.pars.extension]();
		this.pars.reader = imageReader;
		imageReader.read(data);
		
		this.data = imageReader.data();
		
		this.pars.zs = imageReader.zs();
		this.pars.bits = imageReader.bits();
		this.pars.async = imageReader.async();
		this.pars.width = imageReader.width();
		this.pars.height = imageReader.height();
		
		this.pars.buffered = new Array(this.pars.zs);
		this.pars.loaded = new Array(this.pars.zs);
		this.pars.mnmx = new Array(this.pars.zs);
		this.pars.minmaxed = new Array(this.pars.zs);
		
		if (this.pars.async) {
			for (var i = 0; i < this.pars.zs; i++) {
				this.pars.buffered[i] = false;
				this.pars.loaded[i] = false;
				this.pars.minmaxed[i] = false;
			}
		}
		else {
			for (var i = 0; i < this.pars.zs; i++) {
				this.pars.buffered[i] = false;
				this.pars.loaded[i] = true;
				this.pars.minmaxed[i] = false;
			}
		}
		
		if (this.pars.z >= this.pars.zs || this.pars.z < 0) {this.midz();}
		this.loadz(this.pars.z);
		this.size();
		this.setcanvas();
		
		var lod = this.justloaded;
		this.justloaded = false;
		this.hasfile = true;
		this.callFn(this.openFn, this.parameters(), lod);
		
		this.update();
		this.loadzs();

		return this;
	},
	
	scaletowindow: function() {
		var win = this.window();
		var scale = (win[0]/this.pars.width < win[1]/this.pars.height) ? win[0]/this.pars.width : win[1]/this.pars.height;
		this.zoom(scale);
		this.z(Math.round(this.pars.zs/2));
	},
	
	window: function() {
		var w = 600; var h = 500;
		if (document.body.offsetWidth) {w = document.body.offsetWidth; h = document.body.offsetHeight;}
		else if (window.innerWidth) {w = window.innerWidth; h = window.innerHeight;}
		return [w, h];
	},
	
	loaded: function() {
		for (var i = 0; i < this.loadFn.length; i++) {this.loadFn[i]();}
	},
	
	fullyloaded: function() {
		for (var i = 0; i < this.fullLoadFn.length; i++) {this.fullLoadFn[i]();}
	},
	
	progress: function(v) {
		for (var i = 0; i < this.progressFn.length; i++) {this.progressFn[i](v);}
	},
	
	keydown: function(e) {
		if (this.go()) {
			var tar = e.target || e.srcElement;
			if (this.keyFn[e.keyCode] !== undefined && tar.tagName != "TEXTAREA" && tar.tagName != "INPUT") {this.keyFn[e.keyCode]();}
		}
	},
	
	keyup: function(e) {
		
	},
	
	click: function(e) {this.callFn(this.clickFn, this.pars.pos);},
	
	mousemove: function(e) {
		if (this.timer() && this.go()) {
			var off = this.offset();
			
			var x = 0, y = 0;
			if (!e) {var e = window.event;}
			if (e.pageX || e.pageY) {x = e.pageX; y = e.pageY}
			else if (e.clientX || e.clientY) {
				x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
				y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
			}
				
			x = (x - off[0])/this.pars.zoom; y = (y - off[1])/this.pars.zoom;
			if (x < 0) {x = 0;}
			else if (x > this.pars.width) {x = this.pars.width - 1;}
			if (y < 0) {y = 0;}
			else if (y > this.pars.height) {y = this.pars.height - 1;}
			
			this.pars.pos = [Math.floor(x), Math.floor(y), this.pars.z];
			this.callFn(this.moveFn, this.pars.pos);
			if (this.pars.rightclickdown) {this.pars.mmoffset = [x/this.pars.width, y/this.pars.height]; this.update(true);}
		}
	},
	
	offset: function(calc) {
		if (calc || this.pars.offsets === undefined) {
			this.pars.offsets = this.divposition(this.pars.containerdiv);
			return this.pars.offsets;
		}
		else {return this.pars.offsets;}
	},
	
	divposition: function(el) {
		if (typeof el == 'string') {el = document.getElementById(el);}
		var x = parseInt(window.getComputedStyle(im.pars.containerdiv).getPropertyValue('border-left-width')) + 1;
		var y = parseInt(window.getComputedStyle(im.pars.containerdiv).getPropertyValue('border-top-width')) + 1;
		while (el != null) {
			x += el.offsetLeft; y += el.offsetTop;
			el = el.offsetParent;
		}
		return [x, y];
	},
	
	mouseover: function(e) {
		this.pars.over = true;
		this.callFn(this.overFn, this.pars.over);
	},
	
	mouseout: function(e) {
		this.pars.over = false;
		this.callFn(this.overFn, this.pars.over);
	},
	
	pos: function() {
		if (this.pars.over) {return this.pars.pos;}
		else {return false;}
	},
	
	loadz: function(z) {
		if (this.pars.reader != false) {
			if (this.pars.loaded[z] == false) {
				this.pars.reader.z(z);
				this.pars.loaded[z] = true;
				this.minmax(z);
				this.loadingstatus();
			}
		}
		return this;
	},
	
	loadzs: function() {
		var i = 0, found = false;
		while (i < this.pars.zs && !found) {
			if (!this.pars.loaded[i]) {
				var self = this;
				found = true;
				(function(z) {
					setTimeout(function(){self.loadz(z); self.loadzs();}, 2);
				})(i);
			}
			i++;
		}
		return this;
	},
	
	loadingstatus: function() {
		var ts = 0;
		for (i = 0; i < this.pars.loaded.length; i++) {if (this.pars.loaded[i]) {ts++;}}
		this.progress(ts/this.pars.loaded.length);
		if (ts == this.pars.loaded.length) {this.fullyloaded();}
	},
	
	docbinding: function() {
		var self = this;
		document.addEventListener("DOMContentLoaded", function(e) {self.loaded();}, false);
		document.addEventListener("keydown", function(e) {self.keydown(e);});
		document.addEventListener("keyup", function(e) {self.keyup(e);});
	},
	
	containerbinding: function() {
		var self = this;
		this.pars.containerdiv.addEventListener("click", function(e) {self.click();}, true);
		this.pars.containerdiv.addEventListener("mousedown", function(e) {if (e.button == 2) {self.pars.rightclickdown = true; e.preventDefault(); e.stopPropagation();}});
		this.pars.containerdiv.addEventListener("contextmenu", function(e) {e.preventDefault(); e.stopPropagation();});
		this.pars.containerdiv.addEventListener("mouseup", function(e) {if (e.button == 2) {self.pars.rightclickdown = false;}});
		this.pars.containerdiv.addEventListener("mousemove", function(e) {self.mousemove();}, true);
		this.pars.containerdiv.addEventListener("mouseover", function(e) {self.mouseover();}, true);
		this.pars.containerdiv.addEventListener("mouseout", function(e) {self.mouseout();}, true);
	},
	
	shader: function(txt, type) {
		var gl = this.ctx;
		var out = gl.createShader(type == 'v' ? gl.VERTEX_SHADER : gl.FRAGMENT_SHADER);
		gl.shaderSource(out, txt);
		gl.compileShader(out);
		if (!gl.getShaderParameter(out, gl.COMPILE_STATUS)) {alert('ERROR: Problem loading shader. Alert Arthur with '+gl.getShaderInfoLog(out)); gl.deleteShader(out);}
		return out;
	},
	
	fshader: function(type) {
		var t1 = "#ifdef GL_FRAGMENT_PRECISION_HIGH\nprecision highp float;\n#else\nprecision mediump float;\n#endif\nuniform vec2 u_resolution;uniform vec2 u_range_min;uniform sampler2D u_tex;void main(){vec2 texCoord = gl_FragCoord.xy/u_resolution;vec4 floatColor = (texture2D(u_tex, texCoord) - vec4(u_range_min[1], 0, 0, 0))/vec4(u_range_min[0], 0, 0, 0);";
		var tvar = "gl_FragColor = vec4(floatColor[0], floatColor[0], floatColor[0], 1);";
		if (type !== undefined && type == 'exp') {tvar = "if (floatColor[0] >= 0.) {gl_FragColor = vec4(floatColor[0]*floatColor[0], floatColor[0]*floatColor[0], floatColor[0]*floatColor[0], 1);} else {gl_FragColor = vec4(0, 0, 0, 1);}";}
		return this.shader(t1+tvar+"}", 'f');
	},
	
	program: function(fshadertype) {
		var gl = this.ctx;
		var vs = this.shader("attribute vec2 a_pos;void main(){gl_Position=vec4(a_pos,0,1);}", 'v');
		var fs = this.fshader(fshadertype);
		var out = gl.createProgram();
		gl.attachShader(out, vs);
		gl.attachShader(out, fs);
		gl.linkProgram(out);
		if (!gl.getProgramParameter(out, gl.LINK_STATUS)) {alert('ERROR: Problem loading program. Alert Arthur with '+gl.getProgramInfoLog(out)); gl.deleteProgram(out);}
		gl.useProgram(out);
		return out;
	},
	
	gl: function(fshadertype) { // fshadertype can be blank for lienar or 'exp' for exponential
		this.pars.gl = true;
		this.pars.fshadertype = fshadertype;
		var gl = this.ctx;
		var pro = this.program(fshadertype);
		
		var a_pos = gl.getAttribLocation(pro, "a_pos");
		gl.bindBuffer(gl.ARRAY_BUFFER, gl.createBuffer());
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([-1, -1, 1, -1, -1, 1, -1, 1, 1, -1, 1, 1]), gl.STATIC_DRAW);
		gl.enableVertexAttribArray(a_pos);
		gl.vertexAttribPointer(a_pos, 2, gl.FLOAT, false, 0, 0);
		
		this.pars.locsize = gl.getUniformLocation(pro, "u_resolution");
		this.pars.locrm = gl.getUniformLocation(pro, "u_range_min");
		
		this.pars.texture = gl.createTexture();
		gl.bindTexture(gl.TEXTURE_2D, this.pars.texture);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		this.pars.glpixels = new Float32Array(this.pars.width*this.pars.height);
		this.pars.lastdrawnz = -1;
		
		this.ctx.viewport(0, 0, this.pars.width*this.pars.zoom, this.pars.height*this.pars.zoom);
		this.ctx.uniform2f(this.pars.locsize, this.pars.width*this.pars.zoom, this.pars.height*this.pars.zoom);
		
		this.update = this.glupdate;
		this.update();
		return this;
	},
	
	glupdate: function(force) {
		if (force || this.imagetimer()) {
			if (!this.pars.minmaxed[this.pars.z]) {this.minmax(this.pars.z);}
			var gl = this.ctx;
			var ran = this.pars.mnmx[this.pars.z][1] - this.pars.mnmx[this.pars.z][0];
			
			gl.uniform2f(this.pars.locrm, ran*(1 - this.pars.mmoffset[0]), this.pars.mnmx[this.pars.z][0] + ran*this.pars.mmoffset[1]);
			gl.bindTexture(gl.TEXTURE_2D, this.pars.texture);
			if (this.pars.z != this.pars.lastdrawnz) {
				for (var y = 0; y < this.pars.height; y++) {
					var revy = this.pars.height - y - 1;
					for (var x = 0; x < this.pars.width; x++) {
						this.pars.glpixels[y * this.pars.width + x] = this.data[this.pars.z][revy][x];
					}
				}
				this.pars.lastdrawnz = this.pars.z;
			}
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.LUMINANCE, this.pars.width, this.pars.height, 0, gl.LUMINANCE, gl.FLOAT, this.pars.glpixels);
			gl.drawArrays(gl.TRIANGLES, 0, 6);
		}
		this.callFn(this.updateFn);
	},
	
	update: function() {
		if (this.pars.displayMult) {this.im2mult();}
		else {this.im2canvas();}
		this.callFn(this.updateFn);
	},
	
	minmax: function(z, force) {
		if (!this.pars.minmaxed[z] || force !== undefined) {
			if (!this.pars.loaded[this.pars.z]) {this.loadz(this.pars.z);}
			for (var y = 0; y < this.pars.height; y++) {
				for (var x = 0; x < this.pars.width; x++) {
					if (y == 0 && x == 0) {var mn = this.data[z][y][x]; var mx = this.data[z][y][x];}
					if (this.data[z][y][x] > mx) {mx = this.data[z][y][x];}
					if (this.data[z][y][x] < mn) {mn = this.data[z][y][x];}
				}
			}
			this.pars.mnmx[z] = [mn, mx, 255./(mx - mn)];
			this.pars.minmaxed[z] = true;
		}
		return this;
	},
	
	// Once upscaling is fixed, use this function
/*	im2canvas: function() {
		if (!this.pars.minmaxed) {this.minmax(this.pars.z);}
		var i = this.ctx.createImageData(this.pars.width, this.pars.height);
		for (y = 0; y < this.pars.height; y++) {
			for (x = 0; x < this.pars.width; x++) {
				var p = (x + y*this.pars.width)*4;
				var v = (this.data[this.pars.z][y][x] - this.pars.mnmx[this.pars.z][0])*this.pars.mnmx[this.pars.z][2];
				i.data[p+0] = v;
				i.data[p+1] = v;
				i.data[p+2] = v;
				i.data[p+3] = 0xff;
			}
		}
		this.ctx.putImageData(i, 0, 0);
		return this;
	},
*/
	im2canvas: function() {
		if (this.imagetimer()) {
			if (!this.pars.minmaxed[this.pars.z]) {this.minmax(this.pars.z);}
			for (y = 0; y < this.pars.height; y++) {
				for (x = 0; x < this.pars.width; x++) {
					var p = (x + y*this.pars.width)*4;
					var v = (this.data[this.pars.z][y][x] - this.pars.mnmx[this.pars.z][0])*this.pars.mnmx[this.pars.z][2];
					v = Math.floor(v);
					this.ctx.fillStyle = "rgb(" + v + "," + v + "," + v + ")";
					this.ctx.fillRect(x*this.pars.zoom, y*this.pars.zoom, this.pars.zoom, this.pars.zoom);
				}
			}
		}
		return this;
	},
	
	// FIX im2mult!!!! Need to make it zoomable
	im2mult: function() {
		if (!this.pars.minmaxed) {this.minmax(this.pars.z);}
		var i = this.ctx.createImageData(this.pars.width, this.pars.height);
		var max = (this.pars.mnmx[this.pars.z][1] - this.pars.mnmx[this.pars.z][0])*(this.pars.mnmx[this.pars.z][1] - this.pars.mnmx[this.pars.z][0]);
		var ran = 255./(max - this.pars.mnmx[this.pars.z][0]);
		for (y = 0; y < this.pars.height; y++) {
			for (x = 0; x < this.pars.width; x++) {
				var p = (x + y*this.pars.width)*4;
				var v = (this.data[this.pars.z][y][x] - this.pars.mnmx[this.pars.z][0])*this.pars.mnmx[this.pars.z][2];
				var v2 = (this.data[this.pars.z][y][x] - this.pars.mnmx[this.pars.z][0])*(this.data[this.pars.z][y][x] - this.pars.mnmx[this.pars.z][0])*ran;
				i.data[p+0] = v2;
				i.data[p+1] = v2;
				i.data[p+2] = v - v2;
				i.data[p+3] = 0xff;
			}
		}
		this.ctx.putImageData(i, 0, 0);
		return this;
	},
	
	setcanvas: function() {
//		this.ctx = this.pars.imagediv.getContext('2d');
		try {
			this.ctx = this.pars.imagediv.getContext('experimental-webgl');
			this.float32 = this.ctx.getExtension('OES_texture_float');
		} catch(e){}
		if (!this.ctx) {this.ctx = this.pars.imagediv.getContext('2d');}
		else {this.gl();}
	},

	size: function() {
		this.pars.imagediv.setAttribute('width', this.pars.width*this.pars.zoom);
		this.pars.markdiv.setAttribute('width', this.pars.width*this.pars.zoom);
		this.pars.imagediv.setAttribute('height', this.pars.height*this.pars.zoom);
		this.pars.markdiv.setAttribute('height', this.pars.height*this.pars.zoom);
		
		if (this.pars.gl) {
			this.ctx.viewport(0, 0, this.pars.width*this.pars.zoom, this.pars.height*this.pars.zoom);
			this.ctx.uniform2f(this.pars.locsize, this.pars.width*this.pars.zoom, this.pars.height*this.pars.zoom);
		}
		
		this.pars.imagediv.style.width = this.pars.width*this.pars.zoom;
		this.pars.containerdiv.style.width = this.pars.width*this.pars.zoom;
		this.pars.markdiv.style.width = this.pars.width*this.pars.zoom;
		this.pars.imagediv.style.height = this.pars.height*this.pars.zoom;
		this.pars.containerdiv.style.height = this.pars.height*this.pars.zoom;
		this.pars.markdiv.style.height = this.pars.height*this.pars.zoom;

		this.offset(true);

		return this;
	},

	extension: function(name) {
		var self = this;
		function allowed(ext) {
			var pass = false, i = 0;
			for (var key in self.filetypes) {
				if (ext == key) {pass = true;}
				i++;
			}
			return pass;
		}
		
		var ext = name.substring(name.lastIndexOf('.') + 1).toLowerCase();
		if (allowed(ext)) {return ext;}
		else {return false;}
	},
	
	removeextension: function(name) {return name.substring(0, name.lastIndexOf('.'));},
	
	merge: function(a, b, tonew) {
		if (tonew === undefined || tonew === true) {a = this.copy(a);}
		b = this.copy(b);
		for (var key in b) {if (b.hasOwnProperty(key)) {a[key] = b[key];}}
		return a;
	},
	
	copy: function(a) {
		var out;
		if (typeof a === "object" && a instanceof Array) {
			out = [];
			for (var i = 0; i < a.length; i++) {out.push(this.copy(a[i]));}
		}
		else if (typeof a === "object" && a !== null) {
			var out = {};
			for (var key in a) {if (a.hasOwnProperty(key)) {out[key] = this.copy(a[key]);}}
		}
		else {out = a;}
		return out;
	},
	
	
	extend: function(o) {
		for (key in o) {ARTimage.prototype[key] = o[key];}
		return this;
	},
	
//	FIX THIS!
	browser: function() {
		if (this.engine === undefined) {
			if (navigator.userAgent.indexOf("WebKit") > -1) {this.engine = "webkit";}
			else if (navigator.userAgent.indexOf("MSIE") > -1) {this.engine = "ie";}
			else {this.engine = "gecko";}
		}
		
		return this.engine;
	},
	
	clear: function() {
		this.hasfile = false;
		this.q = [];
		this.pars.mnmx = [];
		
		this.data = [[[]]];
		this.buffer = [[[]]];
		this.pars.buffered = [];
		
		this.pars.reader = false;
		
		return this;
	},
	
	// Time
	
	timer: function() {
		var now = +new Date();
		if ((now - this.time.last) > this.time.millis) {
			this.time.last = now;
			return true;
		}
		else {return false;}
	},
	
	imagetimer: function() {
		var now = +new Date();
		if ((now - this.time.last) > this.time.millis) {
			this.time.last = now;
			return true;
		}
		else {
			var self = this;
			if (extraUpdateInterval !== undefined) {
				var extraUpdateInterval = window.setInterval(function(){
					var now = +new Date();
					if ((now - self.time.last) > self.time.millis + 3) {
						window.clearInterval(extraUpdateInterval);
						self.update();
					}
				}, self.time.millis + 2);
			}
			return false;
		}
	},
	
	// Sorts
	
	bound: function(o, b) {
		return o < b[0] ? b[0] : o > b[1] ? b[1] : o;
	},
	
	sortAscending0: function(a, b) {
		a = parseInt(a[0].name);
		b = parseInt(b[0].name);
		return (a-b);
	},
	
	sort: function(a, b) {
		regalpha = /[^a-zA-Z ]/g;
		regnumer = /[^0-9]/g;
		a = a[0].name;
		b = b[0].name;
	    var aalph = a.replace(regalpha, "");
	    var balph = b.replace(regalpha, "");
	    if (aalph === balph) {
	        var anumer = parseInt(a.replace(regnumer, ""), 10);
	        var bnumer = parseInt(b.replace(regnumer, ""), 10);
	        return anumer === bnumer ? 0 : anumer > bnumer ? 1 : -1;
	    }
	    else {return aalph > balph ? 1 : -1;}
	},
};

window.im = artim();
})();
