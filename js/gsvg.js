/*
GraphSVG Library copyright 2013 Arthur Sugden for grapheasy.org
Licensed under GPL version 2
*/
"use strict";

var GSVG = function() {}, svg = function(width, height) {
	if (width === undefined || height === undefined) {return false;}
	var out = new GSVG();
	out.begin(width, height);
	return out;
};

GSVG.prototype = {

/* ============================================================================= */
//	IO
/* ============================================================================= */

	append: function(div) {
		if (div === undefined) {return false;}
		if (this.renderqueue.length > 0) {this.render();}
		if (typeof div === "string") {div = document.getElementById(div);}
		div.appendChild(this.root);
		if (this.bounds !== null) {this.derender();}
		return this;
	},
	
	prepend: function(div) {
		if (div === undefined) {return false;}
		if (this.renderqueue.length > 0) {this.render();}
		if (typeof div === "string") {div = document.getElementById(div);}
		div.insertBefore(this.root, div.firstChild);
		if (this.bounds !== null) {this.derender();}
		return this;
	},
	
	html: function(div) {
		if (div === undefined) {return false;}
		if (this.renderqueue.length > 0) {this.render();}
		if (typeof div === "string") {div = document.getElementById(div);}
		if (div.hasChildNodes()) {
			while (div.childNodes.length > 0) {div.removeChild(div.firstChild);} 
		}
		div.appendChild(this.root);
		if (this.bounds !== null) {this.derender();}
		return this;
	},
	
	get: function() {
		if (this.bounds !== null) {this.derender();}
		if (this.renderqueue.length > 0) {this.render();}
		return this.root;
	},
	
	save: function(name) {
		if (name === undefined) {name = 'graph.svg';}
		
		var exp = document.createElement('div');
		exp.appendChild(this.root.cloneNode(true));
		
		var txt = exp.innerHTML;
//		exp.parentNode.removeChild(exp);
		
		var txtblob = new Blob([txt], {type:'text/plain'});
		var dlink = document.createElement('a');
		dlink.download = name;
		dlink.innerHTML = 'Download File';
		
		if (window.webkitURL !== null) {dlink.href = window.webkitURL.createObjectURL(txtblob);}
		else {
			dink.href = window.URL.createObjectURL(txtblob);
			dlink.onclick = function(e){document.body.removeChild(e.target);}
			dlink.style.display = 'none';
			document.body.appendChild(dlink);
		}
		dlink.click();
	},
	
/* ============================================================================= */
// Primitives	
/* ============================================================================= */
	
	circle: function(p1, r, attr) {
		if (p1 === undefined || r === undefined) {return false;}
		var out, off;
		if (r.length === undefined) {
			out = document.createElementNS(this.ns, 'circle');
			off = {'cx':p1[0]+this.re[0], 'cy':p1[1]+this.re[1], 'r':r};
		}
		else {
			out = document.createElementNS(this.ns, 'ellipse');
			off = {'cx':p1[0]+this.re[0], 'cy':p1[1]+this.re[1], 'rx':r[0], 'ry':r[1]};
		}
		if (attr !== undefined && attr.rotate) {
			if (!attr.transform) {attr.transform = '';}
			else {attr.transform = attr.transform.replace(/rotate\(.*\)/g, '');}
			attr.transform += ' rotate('+attr.rotate+','+off.cx+','+off.cy+')';
//			delete attr.rotate;
		}
		this.attributes(out, off);
		this.attributes(out, attr);
		this.pin(out);
		return this;
	},
	
	ellipse: function(p1, r, attr) {return this.circle(p1, r, attr);},
	
	line: function(p1, p2, attr) {
		if (p1 === undefined || p2 === undefined) {return false;}
		if (attr === undefined) {attr = {};}
		if (attr.stroke === undefined) {attr.stroke = 'black';}
		if (attr['stroke-width'] === undefined) {attr['stroke-width'] = 1;}
	
		var out = document.createElementNS(this.ns, 'line'), off = {'x1':p1[0]+this.re[0], 'y1':p1[1]+this.re[1], 'x2':p2[0]+this.re[0], 'y2':p2[1]+this.re[1]};
		this.attributes(out, off);
		this.attributes(out, attr);
		this.pin(out);
		return this;
	},
	
	polygon: function(ps, attr) { // allows 'open' attribute
		if (ps === undefined) {return false;}
		var out = document.createElementNS(this.ns, 'path');
		
		if (ps[0].length === undefined) {
			for (var i = 0; i < ps.length; i++) {ps[i] = [i*(this.w/ps.length), ps[i]];}
		}
		
		var d = 'M '+(ps[0][0]+this.re[0])+' '+(ps[0][1]+this.re[1]);
		for (var i = 1; i < ps.length; i++) {d += ' L '+(ps[i][0]+this.re[0])+' '+(ps[i][1]+this.re[1]);}
		try {delete attr.open;}
		catch (err) {d += ' Z';}
		
		var off = {'d':d};
		this.attributes(out, off);
		this.attributes(out, attr);
		this.pin(out);
		return this;
	},
	
	rectangle: function(p1, dimensions, attr) {
		if (p1 === undefined || dimensions === undefined) {return false;}
		return this.polygon([p1, [p1[0]+dimensions[0], p1[1]], [p1[0]+dimensions[0], p1[1]+dimensions[1]], [p1[0], p1[1]+dimensions[1]]], attr);
	},
	
	text: function(text, p1, attr) {
		if (text === undefined || p1 === undefined) {return false;}
		var out = document.createElementNS(this.ns, 'text');
		var tnode = document.createTextNode(text);
		out.appendChild(tnode);
		var off = {'x':p1[0]+this.re[0], 'y':p1[1]+this.re[1]};
		if (!attr) {attr = {};}
		var al = null;
		if (attr.align !== null) {al = attr.align;}		
		if (attr.rotate) {
			if (!attr.transform) {attr.transform = '';}
			else {attr.transform = attr.transform.replace(/rotate\(.*\)/g, '');}
			attr.transform += ' rotate('+attr.rotate+','+off.x+','+off.y+')';
//			delete attr.rotate;
		}
		this.attributes(out, off);
		this.attributes(out, attr);
		this.pin(out);
		if (al !== null) {this.textalign(out, al);}
		return this;
	},
	
/* ============================================================================= */
// More Objects	
/* ============================================================================= */
	
	gradient: function(stop, type, attr) {
		if (stop === undefined) {return false;}
		else if (attr === undefined && type !== undefined && typeof type == "object") {attr = type; type = 'linear';}
		else if (type === undefined) {type = 'linear';}
		else if (attr === undefined) {attr = {};}
	
		if (type === 'linear') {
			var off = {'x1':'0%', 'y1':'0%', 'x2':'0%', 'y2':'100%'};
			if (attr.orientation) {
				if (attr.orientation === 'horizontal') {off.x2 = '100%'; off.y2 = '0%';}
				else if (attr.orietnation === 'diagonal') {off.x2 = '100%';}
			}
			
			var out = document.createElementNS(this.ns, 'linearGradient');
			this.attributes(out, off);
			this.attributes(out, attr);
			
			var s; attr = {};
			for (var i = 0; i < stop.length; i++) {
				s = document.createElementNS(this.ns, 'stop');
				if (stop[i].length !== undefined) {
					attr['stop-offset'] = stop[i][0];
					attr['stop-color'] = stop[i][1];
					this.attributes(s, attr);
					attr = {};
				}
				else {
					if (stop[i].offset) {attr['stop-offset'] = stop[i].offset; delete stop[i].offset;}
					if (stop[i].color) {attr['stop-color'] = stop[i].color; delete stop[i].color;}
					this.attributes(s, attr);
					this.attributes(s, stop[i]);
					attr = {};
				}
			}
			
			this.defs(out);
		}
		
		return this;
	},
	
/* ============================================================================= */
// Controls	
/* ============================================================================= */
	
	boundingbox: function() {
		if (this.bounds === null) {
			this.bounds = document.createElement('div');
			var attr = {'id':'_GSVG_Rendering_Space', 'style':'visibility:hidden;width:0px;height:0px;'};
			this.attributes(this.bounds, attr);
			var bod = document.getElementsByTagName('body')[0];
			bod.appendChild(this.bounds);
			this.bounds.appendChild(this.root);
		}
//		console.log(this.lastin.getBBox(), this.lastin.getBoundingClientRect());		
		return this.lastin.getBoundingClientRect();
	},
	
	css: function(attr) {
		if (attr === undefined) {return this.root.getAttribute('style');}
		else {
			if (attr['class'] !== undefined) {
				this.attributes(this.root, {'class':attr.class});
				delete attr.class;
			}
			if (attr.id !== undefined) {
				this.attributes(this.root, {'id':attr.id});
				delete attr.id;
			}
			
			var s = this.root.getAttribute('style');
			if (s !== null) {
				s = s.split(';');
				for (var i = 0; i < s.length; i++) {
					var l = s[i].split(':');
					if (l.length == 2 && l[0].length > 0 && attr[l[0]] === undefined) {
						attr[l[0]] = l[1];
					}
				}
			}
			
			this.style(this.root, attr);
			return this;
		}
	},
	
	group: function(attr) {
		var out = document.createElementNS(this.ns, 'g');
		this.attributes(out, attr);
		this.pin(out);
		this.group.push(out);
		return this;
	},
	
	recenter: function(margin) { // top, right, bottom, left
		if (margin === undefined && this.orig === null) {return false;}
		else {
			if (!margin) {
				this.w = this.orig[0];
				this.h = this.orig[1];
				this.re = [0, 0];
			}
			else if (this.w - margin[1] - margin[3] > 0 && this.h - margin[0] - margin[2] > 0) {
				this.re[0] += margin[3];
				this.re[1] += margin[0];
				this.w -= margin[1] + margin[3];
				this.h -= margin[0] + margin[2];
			}
			return this;
		}
	},
	
	remove: function() {
		if (this.group.length < 1) {this.root.removeChild(this.lastin);}
		else {this.group[this.group.length-1].removeChild(this.lastin);}
		return this;
	},
	
	ungroup: function() {
		this.group.splice(this.group.length - 1);
		return this;
	},

/* ============================================================================= */
// New Graphs BASED ON RENDERING OUTPUT AT THE END
/* ============================================================================= */
	// Notes: bar graphs must be converted to two values, x and y. Everything must have data that, if its a list, is a two-deep list with length > 0.
	// Eventually, add a check if plotting on a grid. Grid type plots can only be on grid type plots, etc. Scatter polar would be different from scatter grid. Now, everything is grid based.
	// Eventually, add the possibility of the axis labels on top and right in addition to just left and bottom.
	// Add a generalized equation to each axis. This is easy, the automatic ticks are harder.

	graphscatter: function(ps, ps2, attr) { // can submit two lists, x and y, or one list of [x,y] pairs. attr can contain fill, stroke, stroke-width, axis parameters
		if (ps2 instanceof Array) {
			var out = [];
			for (var i = 0; i < ps.length; i++) {out.push([ps[i], ps2[i]]);}
			ps = out;
		}
		else if (typeof ps2 == "object" && attr === undefined) {attr = ps2;}
		if (attr === undefined) {attr = {};}
		else {this.pruneattr(attr);}
		
		var self = this;
		this.renderqueue.push({'type':'scatter', 'fn':self.render_scatter, 'data':[ps], 'attr':attr});
		return this;
	},
	
	graphline: function(fnorps, ps2, e1, e2, attr) { // stroke, stroke-width, axis parameters, takes a function or a list of points to connect. Use scatter to plot dots over connected lines.
		var input = this.computeinput2d(fnorps, ps2, e1, e2, attr);
		var lineattr = this.merge({'stroke':'blue', 'stroke-width':2, 'fill':'none'}, input.attributes);
		var	errattr = {'type':'area'};
		if ('error' in lineattr) {errattr = this.merge(errattr, lineattr.error); delete lineattr['error'];}
		
		var self = this;
		if (input.type === "points" && input.errors.length > 0) {this.renderqueue.push({'type':'error', 'fn':self.render_error, 'data':input.errors, 'attr':errattr});}
		
		if (input.type === "function") {this.renderqueue.push({'type':'function', 'fn':self.render_function, 'data':[input['function']], 'attr':lineattr});}
		else if (input.type === "points") {this.renderqueue.push({'type':'line', 'fn':self.render_line, 'data':[input.points], 'attr':lineattr});}
		return this;
	},
	
	axis: function(attr) {this.pruneattr(attr); return this;},
	
	render: function() { // optional. Call if you want to draw on top of graphs.
		this.render_axes();
		for (var i = 0; i < this.renderqueue.length; i++) {
			this.renderqueue[i].fn(this.renderqueue[i].data, this.renderqueue[i].attr, this);
		}
		this.renderqueue = [];
		return this;
	},
	
	clear: function() {
		this.renderqueue = [];
		this.pars = {'axes':{}};
		return this;
	},
	
	// INTERNAL
	
	// Compute input of up to two dimensions of points and error and attributes
	computeinput2d: function(ip1, ip2, ie1, ie2, iattr) {
		if (ip1 === undefined) {return false;}
		
		var test = [ip1, ip2, ie1, ie2, attr], ninputs = test.length;
		while (ninputs > 1 && test[ninputs-1] === undefined) {ninputs--;}
		
		if (typeof test[ninputs-1] === "object" && !(test[ninputs-1] instanceof Array)) {
			var attr = this.copy(test[ninputs - 1]);
			this.pruneattr(attr);
			ninputs--;
		}
		else {var attr = {};}
		
		var totinputs = 0;
		for (var i = 0; i < ninputs; i++) {
			if (test[i] instanceof Array && test[i][0] instanceof Array) {totinputs += 2;}
			else {totinputs++;}
		}
		
		var type = 'points', ps = [], es = [], fn = function(){};
		try {
			if (typeof ip1 === "function") {
				type = 'function';
				fn = ip1;
			}
			else {
				switch(totinputs) {
					case 1:
						ps = this.xcounts(ip1);
						break;
					case 2:
						if (ninputs === 1) {ps = ip1;}
						else if ('error' in attr) {ps = this.xcounts(ip1); es = ip2;}
						else {ps = this.zip(ip1, ip2);}
						break;
					case 3:
						if (ninputs === 3) {ps = this.zip(ip1, ip2); es = ie1;}
						else {ps = ip1; es = ip2;}
				}
			}
		}
		catch (e) {return false;}
		
		if (es.length > 0) {
			for (var extremes = [], i = 0; i < es.length; i++) {extremes.push([ps[i][0], ps[i][1] + es[i]]);}
			for (var i = es.length - 1; i >= 0; i--) {extremes.push([ps[i][0], ps[i][1] - es[i]]);}
		}
		return {'type':type, 'points':ps, 'function':fn, 'errors':[extremes, ps, es], 'attributes':attr};
	},
	
	zip: function(arrin, a2, a3, a4, a5, a6, a7, a8, a9, a10) { // takes an array M of N arrays and converts it to an array N of length M arrays
		var input = [],  test = [a2, a3, a4, a5, a6, a7, a8, a9, a10], out = [];
		if (arrin[0] instanceof Array) {for (var i = 0; i < arrin.length; i++) {input.push(arrin[i]);}}
		else {input.push(arrin);}
		for (var i = 0; i < test.length; i++) {if (test[i] !== undefined) {input.push(test[i]);}}
		
		for (var i = 0; i < input[0].length; i++) {
			var ln = [];
			for (var j = 0; j < input.length; j++) {
				ln.push(input[j][i]);
			}
			out.push(ln);
		}
		return out;
	},
	
	xcounts: function(arrin) {
		var out = [];
		for (var i = 0; i < arrin.length; i++) {out.push([i, arrin[i]]);}
		return out;
	},
	
	pruneattr: function(attr) {
		for (var key in attr) {if (key in this.axis_defaults) {this.pars.axes[key] = this.copy(attr[key]); delete attr[key];}} // move all axis
		return this;
	},
	
	// ACTUAL GRAPHING, RENDER TYPES
	
	render_axes: function() {
		// Prep
		this.axis_apply_defaults();
		this.axis_ranges();
		this.axis_ticks();
		this.axis_padding();
		
		this.recenter(this.pars.axes.margin);
		// Draw
		var titles = {'title':[this.w/2, 0], 'xtitle':[this.w/2, this.h], 'ytitle':[0, this.h/2]};
		for (var key in titles) {if (this.pars.axes[key]) {this.text(this.pars.axes[key], titles[key], this.pars.axes[key+'attr']);}}
		
		var gm = this.pars.graphmargin;
		this.recenter([0, gm[1], 0, gm[3]]);
		if (this.pars.xticks) {
			for (var i = 0; i < this.pars.xticks.length; i++) {
				var x = this.w*((parseFloat(this.pars.xticks[i]) -  this.pars.xrange[0])/this.pars.xrange[2]), y = this.h - gm[2];
				if (this.pars.axes.xaxis) {this.line([x, y], [x, y + 5], this.pars.axes.ylinesattr);}
				if (this.pars.axes.ylines) {this.line([x, gm[0]], [x, this.h-gm[2]], this.pars.axes.ylinesattr);}
				this.text(this.pars.xticks[i], [x, y + 8], this.pars.axes.xticksattr);
			}
		}
		if (this.pars.axes.xaxis) {this.line([0, this.h-gm[2]], [this.w, this.h-gm[2]], this.pars.axes.xaxisattr);}
		
		this.recenter([gm[0], -gm[1], gm[2], -gm[3]]);
		if (this.pars.yticks) {
			for (var i = 0; i < this.pars.yticks.length; i++) {
				var x = gm[3], y = this.h*(1 - (parseFloat(this.pars.yticks[i]) -  this.pars.yrange[0])/this.pars.yrange[2]);
				if (this.pars.axes.yaxis) {this.line([x, y], [x - 5, y], this.pars.axes.xlinesattr);}
				if (this.pars.axes.xlines) {this.line([gm[3], y], [this.w-gm[1], y], this.pars.axes.xlinesattr);}
				this.text(this.pars.yticks[i], [x - 8, y], this.pars.axes.yticksattr);
			}
		}
		if (this.pars.axes.yaxis) {this.line([gm[3], 0], [gm[3], this.h], this.pars.axes.yaxisattr);}
		
		this.recenter([0, gm[1], 0, gm[3]]);
	},
	
	render_scatter: function(data, attr, self) {
		var r = 5; data = data[0];
		if (attr.size !== undefined) {r = attr.size; delete attr.size;}
		if (attr.radius !== undefined) {r = attr.radius; delete attr.radius;}
		for (var i = 0; i < data.length; i++) {
			var x = self.w*(data[i][0] - self.pars.xrange[0])/self.pars.xrange[2], y = self.h*(1 - (data[i][1] - self.pars.yrange[0])/self.pars.yrange[2]);
			if (x >= 0 && x <= self.w && y >= 0 && y <= self.h) {self.circle([x, y], r, attr);}
		}
	},
	
	render_function: function(data, attr, self) {
		var psl = []; data = data[0];
		for (var ps = [], x = 0; x <= self.w; x++) {
			var rx = (x/self.w)*self.pars.xrange[2] + self.pars.xrange[0];
			var y = self.h*(1 - (data(rx) - self.pars.yrange[0])/self.pars.yrange[2]);//console.log(rx, data(rx), x, y);
			if (x >= 0 && x <= self.w && y >= 0 && y <= self.h) {ps.push([x, y]);}
			else if (ps.length > 0) {psl.push(ps); ps = [];}
		}
		if (ps.length > 0) {psl.push(ps);}
		attr.open = true;
		for (var i = 0; i < psl.length; i++) {self.polygon(psl[i], attr);}
	},
	
	render_line: function(data, attr, self) {
		var psl = []; data = data[0];
		for (var i = 0; i < data.length; i++) {
			var x = self.w*(data[i][0] - self.pars.xrange[0])/self.pars.xrange[2], y = self.h*(1 - (data[i][1] - self.pars.yrange[0])/self.pars.yrange[2]);
			if (x >= 0 && x <= self.w && y >= 0 && y <= self.h) {psl.push([x,y]);}
		}
		attr.open = true;
		self.polygon(psl, attr);
	},
	
	render_error: function(data, attr, self) {
		var type = attr.type; delete attr.type;
		if (type === 'area') {
			attr = self.merge({'fill':'#000000', 'stroke':'none', 'opacity':0.2}, attr);
			data = data[0];
			for (var psl = [], i = 0; i < data.length; i++) {
				var x = self.w*(data[i][0] - self.pars.xrange[0])/self.pars.xrange[2], y = self.h*(1 - (data[i][1] - self.pars.yrange[0])/self.pars.yrange[2]);
				if (x >= 0 && x <= self.w && y >= 0 && y <= self.h) {psl.push([x,y]);}
			}
			self.polygon(psl, attr);
		}
		else {
			attr = self.merge({'fill':'none', 'stroke-width':1, 'stroke':'#000000'}, attr);
			for (var i = 0; i < data[1].length; i++) {
				var x = self.w*(data[1][i][0] - self.pars.xrange[0])/self.pars.xrange[2],
				y = self.h*(1 - (data[1][i][1] - self.pars.yrange[0])/self.pars.yrange[2]),
				yup = self.h*(1 - (data[1][i][1] + data[2][i] - self.pars.yrange[0])/self.pars.yrange[2]),
				ydn = self.h*(1 - (data[1][i][1] - data[2][i] - self.pars.yrange[0])/self.pars.yrange[2]);
				if (x >= 0 && x <= self.w && y >= 0 && y <= self.h) {
					self.line([x, y], [x, yup], attr);
					self.line([x, y], [x, ydn], attr);
					
					if (type === 't') {
						self.line([x - 3, yup], [x + 3, yup], attr);
						self.line([x - 3, ydn], [x + 3, ydn], attr);
					}
				}
			}
		}
	},
	
	
	// AXIS RENDERING
	
	axis_defaults: {
		'xrange':['auto', 'auto'],
		'yrange':['auto', 'auto'],
		'xmin':null,
		'xmax':null,
		'ymin':null,
		'ymax':null,
		'margin':[10, 10, 10, 10], // clockwise from top to match css
		'xticks':null,
		'yticks':null,
		'xtitle':null,
		'ytitle':null,
		'title':null,
		'xticksattr':{'align':'top middle'},
		'yticksattr':{'align':'middle right'},
		'xtitleattr':{'align':'bottom middle'},
		'ytitleattr':{'align':'middle bottom', 'rotate':-90},
		'titleattr':{'align':'top middle'},
		'textattr':{'font-family':'Helvetica, Gill Sans, sans-serif', 'font-size':12, 'fill':'black', 'align':'middle top', 'font-weight':'lighter'},
		'xaxis':true,
		'yaxis':false,
		'xaxisattr':{'stroke':'black', 'stroke-width':1},
		'yaxisattr':{'stroke':'black', 'stroke-width':1},
		'xlines':true,
		'ylines':false,
		'xlinesattr':{'stroke':'#666666', 'stroke-width':1},
		'ylinesattr':{'stroke':'#666666', 'stroke-width':1},
	},
	
	axis_apply_defaults: function() {
		this.pars.axes = this.merge(this.axis_defaults, this.pars.axes);
		var cons = ['xticksattr', 'yticksattr', 'xtitleattr', 'ytitleattr', 'titleattr'];
		for (var i = 0; i < cons.length; i++) {this.pars.axes[cons[i]] = this.merge(this.pars.axes.textattr, this.pars.axes[cons[i]]);}
		return this;
	},
	
	axis_ranges: function() {
		var axes = {'x':0, 'y':1};
		for (var axis in axes) {if (axes.hasOwnProperty(axis)) {
			this.pars[axis+'range'] = ['auto', 'auto'];
			//console.log(this.pars.axes[axis+'range'], this.pars.axes[axis+'min'], this.pars.axes[axis+'max']);
			if (this.pars.axes[axis+'range'] !== null) {this.pars[axis+'range'] = this.pars.axes[axis+'range'];}
			if (this.pars.axes[axis+'min'] !== null) {this.pars[axis+'range'][0] = this.pars.axes[axis+'min'];}
			if (this.pars.axes[axis+'max'] !== null) {this.pars[axis+'range'][1] = this.pars.axes[axis+'max'];}
			//console.log('check', this.pars[axis+'range'], this.pars.axes['ymin']);
			
			var min, max, minmaxed = false;
			if (this.pars[axis+'range'][0] == 'auto' || this.pars[axis+'range'][1] === 'auto') {
				for (var f = true, i = 0; i < this.renderqueue.length; i++) {
					if (this.renderqueue[i].data[0] instanceof Array) {
						for (var j = 0; j < this.renderqueue[i].data[0].length; j++) {
							var d = this.renderqueue[i].data[0][j];
							if (f) {min = d[axes[axis]]; max = d[axes[axis]]; f = false; minmaxed = true;}
							if (min > d[axes[axis]]) {min = d[axes[axis]];}
							if (max < d[axes[axis]]) {max = d[axes[axis]];}
						}
					} 
				}
			}
			
			if (minmaxed) {
				this.pars[axis+'range'][0] = this.pars[axis+'range'][0] == 'auto' ? (min ? min : 0) : this.pars[axis+'range'][0];
				this.pars[axis+'range'][1] = this.pars[axis+'range'][1] == 'auto' ? (max ? max : 0) : this.pars[axis+'range'][0];
				this.pars[axis+'range'].push(this.pars[axis+'range'][1] - this.pars[axis+'range'][0]);
			}
		}}
		return this;
	},
	
	axis_ticks: function() {
		var axes = {'x':null, 'y':null}, p = this.pars;
		for (var axis in axes) {if (axes.hasOwnProperty(axis)) {
			if (p.axes[axis+'ticks'] && typeof p.axes[axis+'ticks'] == "object") {p[axis+'ticks'] = p.axes[axis+'ticks'];}
			else {
				var num = p.axes[axis+'ticks'] ? p.axes[axis+'ticks'] : 8;
				p[axis+'ticks'] = this.ticks(p[axis+'range'][0], p[axis+'range'][1], num, true);
			}
			if (p[axis+'ticks'][0] < p[axis+'range'][0]) {p[axis+'range'][0] = p[axis+'ticks'][0];}
			if (p[axis+'ticks'][p[axis+'ticks'].length - 1] > p[axis+'range'][1]) {p[axis+'range'][1] = p[axis+'ticks'][p[axis+'ticks'].length - 1];}
			p[axis+'range'][2] = p[axis+'range'][1] - p[axis+'range'][0];
		}}
		return this;
	},
	
	axis_padding: function() { // Determine padding
		this.pars.graphmargin = [0, 0, 0, 0];
		var p = [this.w/2, this.h/2];
		
		var titles = {'title':0, 'xtitle':2, 'ytitle':3};
		for (var key in titles) {
			if (this.pars.axes[key]) {
				var box = this.text(this.pars.axes[key], p, this.pars.axes[key+'attr']).boundingbox();
				this.remove();
				this.pars.graphmargin[titles[key]] += titles[key]%2 === 0 ? box.height : box.width;
			}
		}
		
		var ticks = {'xticks':2, 'yticks':3};
		for (key in ticks) {
			if (this.pars[key] && this.pars[key].length > 0) {
				for (var max = 0, i = 0; i < this.pars[key].length; i++) {
					var box = this.text(this.pars[key][i], p, this.pars[key+'attr']).boundingbox();
					this.remove();
					var newsz = ticks[key]%2 === 0 ? box.height : box.width+2;
					max = max < newsz ? newsz : max;
				}
				this.pars.graphmargin[ticks[key]] += max;
			}
		}
		
		return this;
	},
	
/* ============================================================================= */
// Graphs	
/* ============================================================================= */
	
	

/* =============================================================================
   *****************************************************************************
   INTERNALS	
   *****************************************************************************
   ============================================================================= */




/* ============================================================================= */
// GSVG Properties	
/* ============================================================================= */
	
	begin: function(w, h) {
		this.w = w;
		this.h = h;
		var baseattr = {'width':w, 'height':h, 'version':2.0};
		
		this.bounds = null;
		this.flag = 0;
		this.group = [];
		this.ns = 'http://www.w3.org/2000/svg';
		this.re = [0, 0];
		this.orig = [w, h];
		
		this.pars = {'axes':{}};
		this.renderqueue = [];
		
		this.root = document.createElementNS(this.ns, 'svg');
		this.attributes(this.root, baseattr);
		this.lastin = this.root;
		return this;
	},
	
	derender: function() {
		var bod = document.getElementsByTagName('body')[0];
		bod.removeChild(this.bounds);
		this.bounds = null;
	},
	
/* ============================================================================= */
// Object Properties and Attachments	
/* ============================================================================= */
	
	keys: function(obj) {
		var keys = [];
		for (var key in obj) {if (obj.hasOwnProperty(key)) {keys.push(key);}}
		return keys;
	},
	
	attributes: function(obj, attr) {
		var excludes = {'align':null, 'rotate':null};
		var k = this.keys(attr);
		for (var i = 0; i < k.length; i++) {
			if (!(k[i] in excludes)) {obj.setAttribute(k[i], attr[k[i]]);}
		}
		return this;
	},
	
	style: function(obj, attr) {
		var k = this.keys(attr);
		var sty = '';
		for (var i = k.length - 1; i >= 0; i--) {
			sty += k[i]+':'+attr[k[i]]+';';
		}
		obj.setAttribute('style', sty);
		return k;
	},
	
	pin: function(obj) {
		this.lastin = obj;
		if (this.group.length < 1) {this.root.appendChild(obj);}
		else {this.group[this.group.length-1].appendChild(obj);}
		return this;
	},
	
	defs: function(obj) {
		if (this.def === undefined) {
			this.def = document.createElementNS(this.ns, 'defs');
			this.root.insertBefore(this.def, this.root.firstChild);
		}
		this.def.appendChild(obj);
		return this;
	},

/* ============================================================================= */
// String and Input Analysis	
/* ============================================================================= */
	
	valuecolor: function(val, range, type) {
		if (!val) {return false;}
		if (!type && !range && typeof range == "string") {type = range; range = null;}
		if (!range) {range = [0, 100];}
		if (!type) {type = 'bw';}
		
		var r = 0, g = 0, b = 0, i = (val - range[0])/(range[1] - range[0]);
		
		if (val < range[0] || val > range[1]) {r = b = 255;}
		else {
			if (type == 'bw') {r = g = b = 255*i;}
			else if (type == 'rwb') {
				if (i < 0.5) {r = g = 2*i*255; b = 255;}
				else {r = 255; g = b = (2 - 2*i)*255;}
			}
			else if (type == 'rgb') {
				if (i < 0.25) {r = 0; g = 4*i*255; b = 255;}
				else if (i < 0.5) {r = 0; g = 255; b = (2 - 4*i)*255;}
				else if (i < 0.75) {r = (3 - 4*i)*255; g = 255; b = 0;}
				else {r = 255; g = (4 - 4*i)*255; b = 0;}
			}
		}
		
		return '#' + Math.round(r).toString(16) + Math.round(g).toString(16) + Math.round(b).toString(16);
	},
	
	
	edges: function(str) {
		var out = [0, 0, 0, 0];
		if (str !== undefined) {
			if (str.indexOf('top') > -1) {out[0]++;}
			if (str.indexOf('right') > -1) {out[1]++;}
			if (str.indexOf('bottom') > -1) {out[2]++;}
			if (str.indexOf('left') > -1) {out[3]++;}
		}
		return out;
	},
	
	textalign: function(obj, style) {
		if (obj === undefined || style === undefined) {return false;}
		else {
			var x = null, y = null, attr;
			if (style.indexOf('top') > -1) {y = 1;}
			else if (style.indexOf('bottom') > -1) {y = -1;}
			
			if (style.indexOf('left') > -1) {x = -1; attr = {'text-anchor':'start'};}
			else if (style.indexOf('right') > -1) {x = 1; attr = {'text-anchor':'end'};}
			
			if (style.indexOf('middle') > -1 || style.indexOf('center') > -1) {
				if (x === null) {x = 0; attr = {'text-anchor':'middle'};}
				if (y === null) {y = 0;}
			}
			
			if (y === 0 || y === 1) {
				var box = this.boundingbox();
				var by = parseFloat(obj.getAttribute('y'));
				if (y === 0) {by = by + 0.7*box.height/2;}
				else {by = by + 0.7*box.height;}
				attr.y = by;
			}
			
			this.attributes(obj, attr);
			return this;
		}
	},
	
/* ============================================================================= */
// Graphing Subfunctions 
/* ============================================================================= */
	
	translate: function(data, fun) {
		for (var i = 0; i < data.length; i++) {data[i] = fun(data[i]);}
		return data;
	},
	
	rebound: function(data, mm) {
		if (mm === undefined) {mm = this.minmax(data);}
		else {if (mm.length !== undefined) {mm = {min:mm[0], max: mm[1]};}}
		var self = this;
		return this.translate(data, function(val){return self.h - (val - mm.min)/(mm.max - mm.min)*self.h;});
	},
	
	invert: function(data) {
		var self = this;
		return this.translate(data, function(val){return self.h - val;});
	},
	
	merge: function(a, b, tonew) {
		if (tonew === undefined || tonew === true) {a = this.copy(a);}
		b = this.copy(b);
		for (var key in b) {if (b.hasOwnProperty(key)) {a[key] = b[key];}}
		return a;
	},
	
	copy: function(a) {
		var out;
		if (typeof a === "object" && a instanceof Array) {
			out = [];
			for (var i = 0; i < a.length; i++) {out.push(this.copy(a[i]));}
		}
		else if (typeof a === "object" && a !== null) {
			var out = {};
			for (var key in a) {if (a.hasOwnProperty(key)) {out[key] = this.copy(a[key]);}}
		}
		else {out = a;}
		return out;
	},
	
	ticks: function(min, max, num, force) {
		function clean_num(num, round) {
			var x_exp = Math.floor(Math.log(num)/Math.log(10));
			var x_frac = num/Math.pow(10, x_exp);
			var out_frac;
			
			if (round) {
				if (x_frac < 1.5) {out_frac = 1;}
				else if (x_frac < 3) {out_frac = 2;}
				else if (x_frac < 7) {out_frac = 5;}
				else {out_frac = 10;}
			}
			else {
				if (x_frac <= 1) {out_frac = 1;}
				else if (x_frac <= 2) {out_frac = 2;}
				else if (x_frac <= 5) {out_frac = 5;}
				else {out_frac = 10;}
			}
			
			return out_frac*Math.pow(10, x_exp);
		}
		var range, spacing, graph_min, graph_max;
		if (force !== undefined && force === true) {
			range = clean_num(max - min, false);
			spacing = clean_num(range/(num - 1), true);
			graph_min = Math.floor(min/spacing)*spacing;
			graph_max = Math.ceil(max/spacing)*spacing;
		}
		else {
			graph_min = min;
			graph_max = max;
			spacing = (max - min)/num;
		}
		
		var fracs_to_show = Math.max(Math.floor(Math.log(spacing)/Math.log(10)), 0);
		
		var marks = [];
		for (var i = graph_min; i < graph_max + 0.5*spacing; i = i + spacing) {
			var exp = Math.floor(Math.log(i)/Math.log(10));
			if (i/Math.pow(10, exp) == 10) {exp++;}
			if (Math.abs(exp) > 5 && Math.abs(exp) != Infinity) {marks.push((i/Math.pow(10, exp))+"E"+exp);}
			else if (parseInt(i, 10) == parseFloat(i)) {marks.push(parseInt(i, 10));}
			else {marks.push(i.toFixed(fracs_to_show + 1));}
		}
		
		return marks;
	},
};