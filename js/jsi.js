function JSI(){}
JSI.prototype = {
	read: function(data) {
		var out = eval("(" + data + ")");
		
//		this.d = out.data;
		
		this.pars = {
			name: out.name,
			time: out.time,
			width: out.width,
			height: out.height,
			zs: out.zs,
		};

		this.d = new Array(this.pars.zs);
		
		for (var z = 0; z < this.pars.zs; z++) {
			this.d[z] = new Array(this.pars.height);
			for (var y = 0; y < this.pars.height; y++) {
				this.d[z][y] = new Array(this.pars.width);
				for (var x = 0; x < this.pars.width; x++) {
					this.d[z][y][x] = out.data[z][x][y];
				}
			}
		}
		
		return this;
	},
	
	z: function(set) {this.pars.z = set; return this;},
	
	zs: function() {return this.pars.zs;},
	bits: function() {return this.pars.bits;},
	async: function() {return false;},
	width: function() {return this.pars.width;},
	height: function() {return this.pars.height;},
	id: function() {return this.pars.name;},
	data: function() {return this.d;}
}