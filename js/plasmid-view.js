/*	Plasmid-view.js modifies plasmid.js to take single files as input and eliminate saving.
	It's used to view plasmid locations from one file in another file.
	
*/

"use strict";

Plasmid.prototype.open = function(pars, loaded) {
	this.imagepars = pars;
	this.pars.file = this.image.file();
	this.pars.id = this.image.id();
	this.directory();
	this.image.scaletowindow();
	this.setmode(0);
	this.update();
};

Plasmid.prototype.directory = function() { // Retrieves from localstorage or initializes pars.directory which contains files and ids
	this.pars.directory = [[false, this.imagepars.files[this.pars.file][0].name]];
	return this;
};

Plasmid.prototype.load = function(data, notes, polygons) { // This is called when the first image is loaded. It retrives all of the previously saved plasmids for all files
	this.data = [data];
	this.notes = [notes];
	this.polygons = [polygons];
	return this;
};

Plasmid.prototype.save = function() {return this;};
Plasmid.prototype.view = function() {return this;};

Plasmid.prototype.translate = function(obj) {
	if (obj instanceof Array) {obj = {'x':obj[0], 'y':obj[1], 'z':obj[2]};}
	var ps = this.allpoints();
	for (var i = 0; i < ps.length; i++) {
		var out = {'x':ps[i].x, 'y':ps[i].y, 'z':ps[i].z};
		for (var key in obj) {if (obj.hasOwnProperty(key)) {out[key] += obj[key];}}
		this.pointupdate(i, out);
	}
	this.update();
	return this;
};

Plasmid.prototype.pointswitch = function(p) {
	var o;
	if (Object.prototype.toString.call(p) === "[object Array]") {o = {'x':p[0], 'y':p[1], 'z':p[2], 'r':p[3], 'spanup':p[4], 'spandn':p[5], 'annulus':p[6], 'sum':p[7]};}
	else {o = [p.x, p.y, p.z, p.r, p.spanup, p.spandn, p.annulus, p.sum];}
	return o;
};