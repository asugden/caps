/*
Plasmid copyright 2013 Arthur Sugden
This is an add-on to ARTimage that allows for measurement and saving of point-like intensities
*/

"use strict";

function Plasmid() {} function plasmid(image) {
	if (image === undefined) {return false;}
	var out =  new Plasmid();
	out.image = image;
	out.init();
	return out;
}

Plasmid.prototype = {
	// BASIC FUNCTIONS =============================================================================
	open: function(pars, loaded) { // Links to artimage's open. Called whenever a new file is opened, loaded is true only if it's a new stack
		this.imagepars = pars;
		this.pars.file = this.image.file();
		this.pars.id = this.image.id();
		
		if (loaded) {this.directory().load();}
		else {this.unsave();}
		this.readpars();
		
		this.image.scaletowindow();
		this.setmode(0);
		this.update();
	},
	
	directory: function() { // Retrieves from localstorage or initializes pars.directory which contains files and ids
		this.pars.directoryid = 'd' + this.pars.id[0];
		var d = localStorage.getItem(this.pars.directoryid);
		if (d !== null) {
			this.pars.directory = JSON.parse(d);
			if (this.pars.directory.length < this.image.pars.files.length) {
				if (this.pars.directory.length == 1) {this.reparsedirectory();}
				else {
					alert('The number of files in this directory has changed. Please contact Arthur at 7723888+caps@gmail.com');
					// I'm worried about the ID system currently for ZVI. I don't want to overwrite anything, so I'll take care of it on an individual basis
				}
			}
		}
		else {
			this.pars.directory = [this.pars.id];
			for (var i = 1; i < this.imagepars.files.length; i++) {this.pars.directory.push([false, this.imagepars.files[i][0].name]);}
		}
		// ADDED!!
		this.pars.directory.sort(this.sort);
		return this;
	},
	
	reparsedirectory: function() {
		this.pars.directory = [this.pars.id];
		for (var i = 1; i < this.imagepars.files.length; i++) {this.pars.directory.push([false, this.imagepars.files[i][0].name]);}
		this.pars.directory.sort(this.sort);
		try {localStorage.setItem(this.pars.directoryid, JSON.stringify(this.pars.directory));}
		catch (e) {alert('I think you\'re running out of space. Contact Arthur immediately with the following: ' + e.description);}
	},
	
	sort: function(a, b) {
		var regalpha = /[^a-zA-Z ]/g, regnumer = /[^0-9]/g, a = a[1], b = b[1];
		var aalph = a.replace(regalpha, ""), balph = b.replace(regalpha, "");
		if (aalph === balph) {
			var anumer = parseInt(a.replace(regnumer, ""), 10);
			var bnumer = parseInt(b.replace(regnumer, ""), 10);
			return anumer === bnumer ? 0 : anumer > bnumer ? 1 : -1;
		}
		else {return aalph > balph ? 1 : -1;}
	},
	
	load: function() { // This is called when the first image is loaded. It retrives all of the previously saved plasmids for all files
		this.data = [];
		this.notes = [];
		this.polygons = [];
		for (var i = 0; i < this.pars.directory.length; i++) {
			var a = localStorage.getItem(this.pars.directory[i][0]);
			if (a !== null) {this.data.push(JSON.parse(a));}
			else {this.data.push([]);}
			var b = localStorage.getItem('n'+this.pars.directory[i][0]);
			if (b !== null) {this.notes.push(b);}
			else {this.notes.push('');}
			var c = localStorage.getItem('g'+this.pars.directory[i][0]);
			if (c !== null) {this.polygons.push(JSON.parse(c));}
			else {this.polygons.push([]);}
		}
		return this;
	},
	
	unsave: function() { // This is called when an individual file is loaded. It reloads some data, but that's ok. It's a just-in-case decision
		if (this.pars.directory[this.pars.file][0] === false) {this.pars.directory[this.pars.file] = this.pars.id;}
		var a = localStorage.getItem(this.pars.id[0]);
		if (a !== null) {this.data[this.pars.file] = JSON.parse(a);}
		var b = localStorage.getItem('n'+this.pars.id[0]);
		if (b !== null) {this.notes[this.pars.file] = b;}
		var c = localStorage.getItem('g'+this.pars.id[0]);
		if (c !== null) {this.polygons[this.pars.file] = JSON.parse(c);}
		return this;
	},
	
	save: function() { // Saves both the individual file that was altered and the directory. Again, some just-in-case situations
		try {localStorage.setItem(this.pars.id[0], JSON.stringify(this.data[this.pars.file]));}
		catch (e) {alert('I think you\'re running out of space. Contact Arthur immediately with the following: ' + e.description);}
		
		try {localStorage.setItem('n'+this.pars.id[0], this.notes[this.pars.file]);}
		catch (e) {alert('I think you\'re running out of space. Contact Arthur immediately with the following: ' + e.description);}
		
		try {localStorage.setItem('g'+this.pars.id[0], JSON.stringify(this.polygons[this.pars.file]));}
		catch (e) {alert('I think you\'re running out of space. Contact Arthur immediately with the following: ' + e.description);}
		
		try {localStorage.setItem(this.pars.directoryid, JSON.stringify(this.pars.directory));}
		catch (e) {alert('I think you\'re running out of space. Contact Arthur immediately with the following: ' + e.description);}
	},
	
	readpars: function() { // This function needed to be wedged in late. It makes sure that the correct annulus is used
		if (this.data[this.pars.file].length > 0) {this.pars.annulus = this.pointget(0).annulus;}
		return this;
	},
	
	setinterface: function(o) { // Links the interface keys and divs
		// uses o.sum, o.r, and o.span
		this.pars.notediv = document.getElementById(o.notes);
		delete o.notes;
		var self = this;
		this.pars.notediv.onkeyup = function(){self.noteset();};
		
		this.inter = o;
		this.link();
		return this;
	},
	
	link: function() { // Link the output to the output divs
		var self = this;
		this.image.onmove(function(p){self.update(p);});
		this.image.onopen(function(p, l){self.open(p, l);});
		this.image.onclick(function(p){self.click(p);});
//		this.image.onfullyloaded(function(){self.recount();});
	},
	
	recount: function() {
		for (var i = 0; i < this.data[this.pars.file].length; i++) {this.pointupdate(i, {});}
	},
	
	setperspective: function(id) {
		this.pars.perspective = true;
		this.perspectiveDiv = document.getElementById(id);
		this.perspectiveCtx = this.perspectiveDiv.getContext('2d');
		return this;
	},
	
	
	// NORMAL MODE MOUSE FUNCTIONS =============================================================================
	radius: function(delta) { // Set the radius of the measurement ring for plasmids
		var rbound = [2, 15];
		if (this.pars.over < 0) {this.pars.r = this.bound(this.pars.r + delta, rbound);}
		else {
			var r = this.pointget(this.pars.over).r;
			r = this.bound(r + delta, rbound);
			this.pointupdate(this.pars.over, {'r':r});
		}
		this.save();
		this.update();
		return this;
	},
	
	span: function(delta, which) { // Set the span in z-levels for measurement of the plasmids
		var sbound = [0, 20];
		if (which === undefined) {return false;}
		if (this.pars.over < 0) {
			if (which === 'up') {this.pars.spanup = this.bound(this.pars.spanup + delta, sbound);}
			else {this.pars.spandn = this.bound(this.pars.spandn + delta, sbound);}
		}
		else {
			if (which === 'up') {
				var s = this.pointget(this.pars.over).spanup;
				s = this.bound(s + delta, sbound);
				this.pointupdate(this.pars.over, {'spanup':s});
			}
			else {
				var s = this.pointget(this.pars.over).spandn;
				s = this.bound(s + delta, sbound);
				this.pointupdate(this.pars.over, {'spandn':s});
			}
		}
		this.save();
		this.update();
		return this;
	},
	
	annulus: function(delta) { // Set the width of the annulus (usally set to 1). This resets ALL PLASMIDS
		var abound = [1, 5];
		this.pars.annulus = this.bound(this.pars.annulus + delta, abound);
		for (var i = 0; i < this.data[this.pars.file].length; i++) {this.pointupdate(i, {'annulus':this.pars.annulus});}
		this.update();
		return this;
	},
	
	// POLYGON MODE FUNCTIONS =============================================================================
	
	
	setmode: function(mode) {
		switch(mode) {
			case 'polygon':
				if (this.pars.mode === 1) {
					this.setmode();
					break;
				}
				this.pars.mode = 1;
				this.update = this.polyupdate;
				this.click = this.polyclick;
				break;
			default:
				this.pars.mode = 0;
				this.update = this.plasupdate;
				this.click = this.plasclick;
				break;
		}
		this.save();
		this.update();
		return this;
	},
	
	plasclick: function(p) {
		var over = this.pointover(p);
		if (over < 0) {this.pointset(p);}
		else {this.pointremove(over);}
		this.update();
	},
	
	polyclick: function(p) { // Allocates mouse clicks
		function dist(p1, p2) {return (p2[0] - p1[0])*(p2[0] - p1[0]) + (p2[1] - p1[1])*(p2[1] - p1[1]) < 19 ? true : false;} // is d < root(18)
		
		if (this.curpoly.length > 1) {
			if (dist(this.curpoly[0], p) || dist(this.curpoly[this.curpoly.length - 1], p)) {
				this.polygons[this.pars.file].push(this.curpoly);
				this.curpoly = [];
				this.save();
				return this;
			}
		}
		else if (this.curpoly.length === 0) {
			var found = false, i = 0;
			while (i < this.polygons[this.pars.file].length && !found) {
				if (this.pointinpoly(p, this.polygons[this.pars.file][i])) {
					this.polygons[this.pars.file].splice(i, 1);
					found = true;
				}
				i++;
			}
			if (found) {this.save(); return this;}
		}
		
		this.curpoly.push(p.slice(0,2));
		this.update();
		return this;
	},
	
	// MODES =============================================================================
	cover: function(num) {
		if (num === this.pars.covermode) {
			this.pars.covermode = -1;
			this.pars.cover.parentNode.removeChild(this.pars.cover);
			for (var i = 0; i < document.body.childNodes.length; i++) {document.body.childNodes[i].className = document.body.childNodes[i].className.replace(' hide', '');}
			return false;
		}
		else {
			if (this.pars.covermode > -1) {
				while (this.pars.cover.lastChild) {this.pars.cover.removeChild(this.pars.cover.lastChild);}
			}
			else {
				this.pars.cover = document.createElement('div');
				this.attributes(this.pars.cover, {'id':'white'});
				this.style(this.pars.cover, {'position':'fixed', 'overflow':'auto', 'background':'#FFFFFF', 'width':'100%', 'height':'100%', 'z-index':21, 'top':0, 'left':0});
				for (var i = 0; i < document.body.childNodes.length; i++) {document.body.childNodes[i].className += ' hide';}
				document.body.appendChild(this.pars.cover);	
			}
			this.pars.covermode = num;
			return true;
		}
	},
	
	summary: function() { // Puts up the summary of all plasmids, calling summarytable and summarygraph
		if (this.cover(0)) {
			var t = document.createElement('table');
			this.attributes(t, {'id':'_sumtable'});
			
			var self = this;
			this.ttext(t, ['FILE', 'INFO'], {'class':'title'});
			for (var i = 0; i < this.data.length; i++) {
				var s = document.createElement('span'), ps = this.polypointsget(i);
				s.innerText = this.pars.directory[i][1] + "\n";
				var tr = this.ttext(t, [[s, this.ptable(ps)], this.psum(ps) + this.notes[i]]);
				(function(tr, i){
					self.attributes(s, {'class':'name'}).attributes(tr, {'name':i});
					tr.addEventListener('click', function(){self.stclick(tr);});
					tr.addEventListener('mouseover', function(){self.stover(tr);});
					tr.addEventListener('mouseout', function(){self.stout(tr);});
				})(tr, i);
			}
			this.pars.cover.appendChild(t);
		}
		return this;
	},
	
	stclick: function(tr) {this.summary().image.open(parseInt(tr.getAttribute('name'), 10));},
	stover: function(tr) {for (var i = 0; i < tr.childNodes.length; i++) {tr.childNodes[i].className = "over";}},
	stout: function(tr) {for (var i = 0; i < tr.childNodes.length; i++) {tr.childNodes[i].className = "";}},
	
	colors: function(i) {
		var out = ['6A97C4', '6AC4A5', 'C46AB2', 'C4B76A', '6F6AC4', 'D17F36', '6AC0C4', 'A66AC4'];
		return out[i%out.length];
	},
	
	ptable: function(cells) {
		var t = document.createElement('table');
		this.attributes(t, {'class':'ptable'});
		for (var i = 0; i < cells.length; i++) {
			for (var j = 0; j < cells[i].length; j++) {
				var p = cells[i][j], clr = {'style':'color:#'+this.colors(i)};
				if (!p.sum) {p.sum = -1;}
				this.ttext(t, ['X:', p.x, 'Y:', p.y, 'Z:', p.z, 'R:', p.r, 'S:', p.spanup+'&#8593;'+p.spandn+'&#8595;', '=', p.sum.toFixed(2)], [{}, clr]);
			}
		}
		return t;
	},
	
	psum: function(ps) {
		var out = '';
		for (var i = 0; i < ps.length; i++) {
			for (var sum = 0, j = 0; j < ps[i].length; j++) {sum += ps[i][j].sum;}
			out += '<span style="color:#'+this.colors(i)+'">' + this.commas(sum) + ' from ' + ps[i].length + ' plasmids</span><br />';
		}
		return out;
	},
	
	summarygraph: function() { // Create the graph of intensities over time
		
	},
	
	del: function(id) { // delete an element if such an element exists
		var e = document.getElementById(id);
		if (e !== null) {e.parentNode.removeChild(e);}
		return this;
	},
	
	ttext: function(parent, txt, attr, attrl) { // txt can be an array or a single element, if txt is a list, attrl 
		var self = this;
		function cell(p, t, a) {
			var o = document.createElement('td');
			if (a !== undefined) {self.attributes(o, a);}
			if (typeof t === "string") {o.innerHTML = t;}
			else if (typeof t === "number") {o.innerHTML = t;}
			else if (typeof t === "object") {
				if (t instanceof Array) {for (var j = 0; j < t.length; j++) {o.appendChild(t[j]);}}
				else {o.appendChild(t);}
			}
			p.appendChild(o);
			return o;
		}
		
		if (txt.length === undefined) {return cell(parent, txt, attr);}
		else {
			attrl = (attrl === undefined) ? attr instanceof Array ? attr : [] : attrl;
			attr = attr instanceof Array ? {} : attr;
			var r = document.createElement('tr');
			if (attr !== undefined) {this.attributes(r, attr);}
			for (var i = 0; i < txt.length; i++) {cell(r, txt[i], attrl[i%attrl.length]);}
			parent.appendChild(r);
			return r;
		}
	},
	
	view: function() {
		var v = window.open('view.html', '_blank');
		var self = this;
		v.addEventListener('load', function(){
			v.passed.data = self.copy(self.data[self.pars.file]);
			v.passed.notes = self.notes[self.pars.file];
			v.passed.polygons = self.copy(self.polygons[self.pars.file]);
		});
		return this;
	},
	
	polypointsget: function(t) {
		var ps = this.allpoints(t), gons = this.polygons[t];
		var i = 0, out = [];
		while (ps.length > 0 && i < gons.length) {
			var newgon = true, j = ps.length - 1;
			while (j >= 0) {
				if (this.pointinpoly([ps[j].x, ps[j].y], gons[i])) {
					if (newgon) {out.push([]); newgon = false;}
					out[out.length - 1].push(ps.splice(j, 1)[0]);
				}
				j--;
			}
			i++;
		}
		if (ps.length > 0) {out.push(ps);}
		return out;
	},
	
	pointinpoly: function(p, poly) { // Check whether an x,y coordinate lies within a polygon
		var crossings = 0;
		for (var i = 0; i < poly.length; i++) {
			var xa = poly[i][0], xb = poly[(i + 1)%poly.length][0], ya = poly[i][1], yb = poly[(i + 1)%poly.length][1], x1, x2;
			if (xa < xb) {x1 = xa; x2 = xb;}
			else {x1 = xb; x2 = xa;}
			
			if (p[0] > x1 && p[0] <= x2 && (p[1] < ya || p[1] <= yb)) {
				var dx = xb - xa;
				var k = Math.abs(dx) < 0.000001 ? Infinity : (yb - ya)/dx;
				var m = ya - k*xa;
				if (p[1] <= k*p[0] + m) {crossings++;}
			}
		}
		return crossings%2 === 1 ? true : false;
	},
	
	gaussianfits: function(type) { // Puts up the summary of all plasmids, calling summarytable and summarygraph
		function bgsubtract(ds, p) {
			for (var sum = 0, n = 0, i = 0; i < ds.length; i++) {if (ds[i][0] >= p.r) {sum += ds[i][1]; n++;}}
			var bg = sum/n;
			for (i = 0; i < ds.length; i++) {ds[i][1] -= bg;}
			return bg;
		}
		
		function max(ds) {
			for (var out = [], i = 0; i < ds.length; i++) {
				for (var j = 0; j < ds[i].length; j++) {
					if (i == 0) {out.push(ds[i][j]);}
					else if (out[j] < ds[i][j]) {out[j] = ds[i][j];}
				}
			}
			return out;
		}
		function gaussianscale(ds, addto, sigma, max, r) {for (var i = 0; i < ds.length; i++) {addto.push([ds[i][0]/sigma, ds[i][1]/max[1], ds[i][0] < r ? 0 : 1]);}}
		function linearscale(ds, addto, ignorexover, max, scalex) {for (var i = 0; i < ds.length; i++) {addto.push([ds[i][0]/scalex, ds[i][1]/max[1]]);}}
		function combine(ds, addto, r) {for (var i = 0; i < ds.length; i++) {addto.push([ds[i][0], ds[i][1], ds[i][0] < r ? 0 : 1]);}}
		
		
		if (this.cover(1)) {
			var ps = this.allpoints(), combined = [], scaled = [], linscaled = [], gaussian;
			
			for (var i = 0; i < ps.length; i++) {
				var p = ps[i];
				var ds = this.image.cylinder([p.x, p.y, p.z], p.r+p.annulus);
				var bg = bgsubtract(ds, p);
				gaussian = this.fitnormal(ds);
				var sum = this.image.sum([p.x, p.y, p.z], p.r, 0, 0, p.annulus);
				
				combine(ds, combined, p.r);
				gaussianscale(ds, scaled, gaussian.sigma, max(ds), p.r);
				linearscale(ds, linscaled, p.r, max(ds), p.r);
				
				if (type === 'fullheight') {gaussian.bg = bg; gaussian.ymin = 0;}
				this.pars.cover.appendChild(this.radialgraph(ds, this.merge({'r':p.r, 'title':'PLAS | \u03C3:' + gaussian.sigma.toFixed(2) + ' area:'+sum.toFixed(1) + ' fit area:'+(gaussian.a*2*Math.PI*gaussian.sigma*gaussian.sigma).toFixed(1)}, gaussian)));
				//this.pars.cover.appendChild(this.vectorizedplasmid(p));
			}
			
			gaussian = this.fitnormal(combined);
			this.pars.cover.appendChild(this.radialgraph(combined, {'title':'COMBINED \u03C3: ' + gaussian.sigma.toFixed(3), 'a':0, 'mu':0, 'sigma':1.5}));
			this.pars.cover.appendChild(this.radialgraph(scaled, {'title':'GAUSS SCALED, \u03C3: ' + 1.000, 'a':1, 'mu':0, 'sigma':1}));
			for (var subscaled = [], i = 0; i < linscaled.length; i++) {if (linscaled[i][0] < 1) {subscaled.push(this.copy(linscaled[i]));}}
			var line = this.fitline(subscaled);
			
			var lingraph = this.radialgraph(linscaled, this.merge({'title':'LINE SCALED, [0,1] m:'+line.m.toFixed(2)+', b:'+line.b.toFixed(2)+', r:'+line.correlation.toFixed(3)+', n:'+line.n, 'r':1}, line));
			this.pars.cover.appendChild(lingraph);
			
			var self = this;
			linscaled.sort(function(a,b){return a[0]-b[0];});
			lingraph.onclick = function(){self.export(linscaled, self.image.removeextension(self.pars.id[1]));};
			
			
			// Fix this !!!!
			var wc = document.createElement('div');
			var wcdata = this.getwilcoxon();
			wc.innerHTML = 'W: ' + wcdata.W + ' &nbspl z: ' + this.formatn(wcdata.z, 3) + ' &nbsp; tails: ' + wcdata.tails + ' &nbsp; significant: ' + wcdata.significant + ' &nbsp; p-value: ' + this.formatn(wcdata.p, 3);
			this.pars.cover.appendChild(wc);
			
			// Fix this as well!!!
		}
		
		return this;
	},
	
	// Clean this up!!!
	downloadallsums: function() {
		var self = this;
		function testsum(p) {
			var rand = Math.random()*Math.PI*2, rad = p.r*3;
			var x = Math.cos(rand)*rad, y = Math.sin(rand)*rad;
			return [self.image.sum([x, y, p.z], p.r, p.spanup, p.spandn, p.annulus), self.image.sum([x, y, p.z], p.r, p.spanup, p.spandn, 0)];
		}
		
		var ps = this.pointsget();
		var orig = [], test = [], origunsub = [], testunsub = [];
		for (var i = 0; i < ps.length; i++) {
			var tries = 0, t = testsum(ps[i]);
			while (tries < 1000 && t[0] === false) {
				t = testsum(ps[i]);
				tries++;
			}
			if (tries < 1000) {
				orig.push(this.image.sum([ps[i].x, ps[i].y, ps[i].z], ps[i].r, ps[i].spanup, ps[i].spandn, ps[i].annulus));
				origunsub.push(this.image.sum([ps[i].x, ps[i].y, ps[i].z], ps[i].r, ps[i].spanup, ps[i].spandn, 0));
				test.push(t[0]);
				testunsub.push(t[1]);
			}
		}
		console.log('test');
		for (var txt = 'signal-position-subtracted,signal-position-raw,background-position-subtracted,background-position-raw\r\n', i = 0; i < orig.length; i++) {txt += orig[i]+','+origunsub[i]+','+test[i]+','+testunsub[i]+'\r\n';}
		this.export(txt, 'all-sums-'+this.image.removeextension(this.pars.id[1]));
	},
	
	export: function(txt, fname) {
		if (typeof txt !== "string") {txt = this.textify(txt);}
		var txtblob = new Blob([txt], {type:'text/plain'});
		if (fname.toLowerCase().substring(fname.length - 4) !== '.txt') {fname += '.txt';}
		
		var dlink = document.createElement('a');
		dlink.download = fname;
		dlink.innerHTML = 'Download File';
		
		if (window.webkitURL !== null) {dlink.href = window.webkitURL.createObjectURL(txtblob);}
		else {
			dink.href = window.URL.createObjectURL(txtblob);
			dlink.onclick = function(e){document.body.removeChild(e.target);}
			dlink.style.display = 'none';
			document.body.appendChild(dlink);
		}
		dlink.click();
	},
	
	textify: function(obj, layer) {
		var out = "", self = this;
		if (layer === undefined) {layer = 0;}
		if (typeof obj === "string") {out += obj;}
		else if (typeof obj === "number") {out += obj.toString();}
		else if (typeof obj === "object") {
			for (var key in obj) {if (obj.hasOwnProperty(key)) {
				if (parseInt(key).toString() === key) {
					out += this.textify(obj[key], layer+1);
					if (layer === 0) {out += "\r\n";}
					else {out += "\t";}
				}
			}}
		}
		return out;
	},
	
	vectorizedplasmid: function(p) {
		var extendout = 1;
		var d = Math.ceil(p.r) + p.annulus + extendout;
		var out = svg(d*2+1, d*2+1);
		for (var dx = -d; dx <= d; dx++) {
			for (var dy = -d; dy <= d; dy++) {
				var i = Math.floor((this.image.data[p.z][p.y + dy][p.x + dx] - this.image.pars.mnmx[this.image.pars.z][0])*this.image.pars.mnmx[this.image.pars.z][2]);
				out.rectangle([dx + d, dy + d], [1, 1], {'fill':'rgb('+i+','+i+','+i+')'});
			}
		}
		out.circle([d + extendout/2, d + extendout/2], p.r + p.annulus/2, {'fill':'none', 'stroke':'#BF1F1F', 'opacity':0.5, 'stroke-width':p.annulus});
		return out.get();
	},
	
	fitnormal: function(ds) {
		function chi(a, mu, sig) {
			var fn = function(x) {return a*Math.exp(-(x - mu)*(x - mu)/(2*sig*sig));};
			for (var out = 0, i = 0; i < ds.length; i++) {
				var off = ds[i][1] - fn(ds[i][0]);
				out += off*off;
			}
			return out;
		}
		
		function em(trials, step, a, mu, sig) {
			step = chi(a, mu, sig) > chi(a, mu, sig + step) ? step : -step;
			var c = chi(a, mu, sig), cn = chi(a, mu, sig + step), i = 1;
			while (i < trials && cn < c) {
				c = cn;
				sig += step;
				cn = chi(a, mu, sig + i*step);
				i++;
			}
			return {'a':a, 'mu':mu, 'sigma':sig + (i - 1)*step};
		}
		
		for (var sum = 0, i = 0; i < ds.length; i++) {sum += ds[i][0];}
		return em(1000, 0.01, ds[0][1], 0, Math.sqrt(sum/(ds.length - 1)));
	},
	
	fitline: function(ds) {
		var n = ds.length;
		for (var Ex = 0, Ex2 = 0, Exy = 0, Ey = 0, xmin, xmax, i = 0; i < n; i++) {
			if (i == 0) {xmin = ds[i][0]; xmax = ds[i][0];}
			else if (xmin > ds[i][0]) {xmin = ds[i][0];}
			else if (xmax < ds[i][0]) {xmax = ds[i][0];}
			
			Ex += ds[i][0];
			Ex2 += ds[i][0]*ds[i][0];
			Exy += ds[i][0]*ds[i][1];
			Ey += ds[i][1];
		}
		var b = (Ey*Ex2 - Ex*Exy)/(n*Ex2 - Ex*Ex), m = (n*Exy - Ex*Ey)/(n*Ex2 - Ex*Ex);
		
		var xmean = Ex/n, ymean = Ey/n;
		for (var Exxyy = 0, Exx2 = 0, Eyy2 = 0, i = 0; i < n; i++) {
			Exxyy += (ds[i][0] - xmean)*(ds[i][1] - ymean);
			Exx2 += (ds[i][0] - xmean)*(ds[i][0] - xmean);
			Eyy2 += (ds[i][1] - ymean)*(ds[i][1] - ymean);
		}
		var r = Exxyy/Math.sqrt(Exx2*Eyy2);
		
		return {'m':m, 'b':b, 'correlation':r, 'range':[xmin, xmax], 'n':n};
	},
	
	radialgraph: function(ds, pars) {
		var sz = [300, 300];
		if (pars.size !== undefined) {sz = (typeof pars.size === "number") ? [pars.size, pars.size] : pars.size;}
		var out = svg(sz[0], sz[1]);
		
		out.css({'class':'radialgraph'});
		
		// Graph the function
		var fn = (pars.m === undefined) ? (pars.bg === undefined) ? function(x) {return pars.a*Math.exp(-(x - pars.mu)*(x - pars.mu)/(2*pars.sigma*pars.sigma));} : function(x) {return pars.bg + pars.a*Math.exp(-(x - pars.mu)*(x - pars.mu)/(2*pars.sigma*pars.sigma));} : function(x) {if (x >= pars.range[0] && x <= pars.range[1]) {return pars.m*x + pars.b;}}
		out.graphline(fn, {'stroke':'#666666'});
		
		var excludes = {'size':null, 'r':null, 'a':null, 'mu':null, 'sigma':null, 'chi':null, 'bg':null, 'm':null, 'b':null, 'correlation':null, 'range':null}, axisedits = {};
		for (var key in pars) {if (pars.hasOwnProperty(key) && !(key in excludes)) {axisedits[key] = pars[key];}}
		out.axis(axisedits);
		
		for (var ps = [[], []], i = 0; i < ds.length; i++) {
			var addto = 0;
			if (pars.r !== undefined) {addto = ds[i][0] < pars.r ? 0 : 1;}
			else if (ds[0].length > 2) {addto = ds[i][2];}
			ps[addto].push(ds[i]);} // convert ds to ps with inside radius and outside
		
		if (pars.bg !== undefined) {
			for (var j = 0; j < 2; j++) {for (i = 0; i < ps[j].length; i++) {ps[j][i][1] += pars.bg;}}
			out.graphline(function(x) {return pars.bg;}, {'stroke':'#BF1F1F', 'opacity':0.5, 'stroke-width':5});
		}
		
		out.graphscatter(ps[0], {'stroke':'#000000', 'stroke-width':0.5, 'fill':'rgba(33,116,219,0.7)'});
		if (ps[1].length > 0) {out.graphscatter(ps[1], {'stroke':'#000000', 'stroke-width':0.5, 'fill':"rgba(191,31,31,0.7)"});}
		return out.get();
	},
	
//	CALL_STATISTICS ===============================================
	getwilcoxon: function() {
		var self = this;
		function testsum(p) {
			var rand = Math.random()*Math.PI*2, rad = p.r*3;
			var x = Math.cos(rand)*rad, y = Math.sin(rand)*rad;
			return self.image.sum([x, y, p.z], p.r, p.spanup, p.spandn, p.annulus);
		}
		
		var ps = this.pointsget();
		var orig = [], test = [];
		for (var i = 0; i < ps.length; i++) {
			var tries = 0, t = testsum(ps[i]);
			while (tries < 1000 && t === false) {
				t = testsum(ps[i]);
				tries++;
			}
			if (tries < 1000) {
				orig.push(this.image.sum([ps[i].x, ps[i].y, ps[i].z], ps[i].r, ps[i].spanup, ps[i].spandn, ps[i].annulus));
				test.push(t);
			}
		}
		
		return this.wilcoxon(orig, test);
	},	
	
//	STATISTICS ====================================================
	
	wilcoxon: function(arr1, arr2, tails) { // Wilcoxon sign rank test, NOT Wilcoxon rank sum test, also known as Mann-Whitney
		if (arr1.length !== arr2.length || arr1.length < 2 || arr2.length < 2) {return false;}
		if (tails === undefined) {tails = 2;}
		
		var vals = [], ranks = [];
		for (var i = 0; i < arr1.length; i++) {
			var sub = arr2[i] - arr1[i];
			if (sub !== 0) {vals.push([Math.abs(sub), this.sgn(sub)]);}
		}
		vals.sort(function(a,b){return a[0] - b[0];});
		var N = vals.length;
		
		i = 0;
		while (i < N) {
			var j = i + 1;
			while (j < N && vals[j] === vals[i]) {j++;}
			var rank = (i + 1.0 + j)/2;
			for (var k = i; k < j; k++) {ranks.push(rank); i++;}
		}
		
		for (var W = 0, i = 0; i < N; i++) {W += vals[i][1]*ranks[i];}
		W = Math.abs(W);
		
		if (N > 9) {
			var sigmaW = Math.sqrt((N*(N + 1.0)*(2*N + 1))/6);
			var z = (W - 0.5)/sigmaW;
			var p = this.normalcdf(z);
			var sig = tails == 2 ? p < 0.025 ? true : false : p < 0.05 ? true : false;
			return {'z':z, 'tails':tails, 'significant':sig, 'W':W, 'p':p};
		}
		else {
			return {'z':'too few N to calculate without table', 'W':W, 'significant':'unknown'}
		}
	},
	
	normalcdf: function(x, mu, sigma) { // Normal cumulative distribution function, gives p-value from z
		if (mu === undefined) {mu = 0.0;}
		if (sigma === undefined) {sigma = 1.0;}
		
		var out = 0.5*(1.0 + this.erf((x - mu)/Math.sqrt(2*sigma*sigma)));
		
		if (x < 0) {return out;}
		else {return 1 - out;}
	},
	
	// Sign function
	sgn: function(i) {return i < 0 ? -1 : i > 0 ? 1 : 0;},	
	
	// Error function
	erf: function(x) { // Abramowitz, Milton; Stegun, Irene A., eds. (1965), "Chapter 7", Handbook of Mathematical Functions with Formulas, Graphs, and Mathematical Tables, New York: Dover, p. 297, ISBN 978-0486612720, MR 0167642. via Wikipedia, approximation accurate to 1.5E-7
		var sign = 1;
		if (x < 0) {x = 0; sign = -1;}
		
		var p = 0.3275911, a1 = 0.254829592, a2 = -0.284496736, a3 = 1.421413741, a4 = -1.453152027, a5 = 1.061405429; // constants
		var t = 1.0/(1.0 + p*x);
		var out = 1.0 - (a1*t + a2*Math.pow(t, 2) + a3*Math.pow(t, 3) + a4*Math.pow(t, 4) + a5*Math.pow(t, 5))*Math.exp(-x*x);
		return out*sign;
	},
	
	loge: function(x) {return Math.log(x);},
	log10: function(x) {return Math.log(x)/Math.LN10;},
	zeros: function(x) {
		var out = this.log10(Math.abs(x));
		if (x < 1) {return Math.abs(Math.ceil(out)) + 1;}
		else {return Math.floor(out);}
	},
	
	formatn: function(x, precisionForce, eOverTimes, maxPlaces, minPlaces) {
		if (precisionForce === undefined) {
			var precision = 3;
			precisionForce = false;
		}
		else {
			var precision = precisionForce;
			precisionForce = true;
		}
		
		if (eOverTimes === undefined) {eOverTimes = false;}
		if (maxPlaces === undefined) {maxPlaces = 5;}
		if (minPlaces === undefined) {minPlaces = 3;}
		
		var zs = this.zeros(x);
		if (x >= Math.pow(10, maxPlaces)) {
			x = Math.round(x/Math.pow(10, zs - (precision - 1)))/Math.pow(10, (precision - 1));
			if (eOverTimes) {return x.toString() + 'e' + zs;}
			else {return x.toFixed(precision - 1) + '&times;10<sup>' + zs + '</sup>';}
		}
		else if (x < Math.pow(10, -1*minPlaces)) {
			x = Math.round(x*Math.pow(10, zs + (precision - 1)))/Math.pow(10, (precision - 1));
			if (eOverTimes) {return x.toString() + 'e-' + zs;}
			else {return x.toFixed(precision - 1) + '&times;10<sup>-' + zs + '</sup>';}
		}
		else if (precisionForce && x > 1.0) {
			x = Math.round(x/Math.pow(10, zs - (precision - 1)))/Math.pow(10, (precision - 1))*Math.pow(10, zs);
			if ((zs - precision - 1) < 0) {return x.toFixed(precision - zs - 1);}
			else {return Math.round(x).toString();}
		}
		else if (precisionForce && x < 1.0) {
			x = Math.round(x*Math.pow(10, zs + (precision - 1)))/Math.pow(10, (precision - 1))/Math.pow(10, zs);
			return x.toFixed(zs + precision - 1);
		}
		else {return x.toString();}
	},
	
//	CORE ==========================================================
	
	plasupdate: function(p) {
		if (p === undefined) {p = this.image.pos();}
		var o = {'r':this.pars.r, 'spanup':this.pars.spanup, 'spandn':this.pars.spandn, 'sum':'-,---,---.-', 'csum':'0,000,000', 'pn':0};
		if (this.data !== undefined && this.data.length > 0) {o.csum = this.pointsum(); o.pn = this.data[this.pars.file].length;}
		this.pars.notediv.value = this.notes[this.pars.file];
		
		this.mark(p);
		if (p) {
			if (this.pars.over < 0) {
				if (this.pars.perspective) {this.perspective();}
				var s = this.image.sum(p, this.pars.r, this.pars.spanup, this.pars.spandn, this.pars.annulus);
				if (s) {o.sum = s.toFixed(1);}
			}
			else {
				var fp = this.pointget(this.pars.over);
				o.r = fp.r; o.spanup = fp.spanup; o.spandn = fp.spandn;
				var s = this.image.sum([fp.x, fp.y, fp.z], fp.r, fp.spanup, fp.spandn, fp.annulus);
				if (s) {o.sum = s;}
				if (this.pars.perspective) {this.perspective(fp);}
			}
		}
		for (var key in this.inter) {if (this.inter.hasOwnProperty(key)) {document.getElementById(this.inter[key]).innerHTML = this.commas(o[key]);}}
		document.getElementById(this.inter.r).innerHTML = this.commas(o.r, 1);
		return this;
	},
	
	polyupdate: function(p) {
		this.image.cleardraw();
		if (this.curpoly.length > 0) {
			this.image.drawcircle(this.curpoly[0], 1.5, 3, 'rgba(204, 37, 37, 0.7)');
			this.image.drawpath(this.curpoly, 'rgba(204, 37, 37, 0.7)');
		}
		for (var i = 0; i < this.polygons[this.pars.file].length; i++) {
			this.image.drawpolygon(this.polygons[this.pars.file][i], 'rgba(237, 207, 26, 0.7)');
		}
		return this;
	},
	
	update: function(p) {},
	
	mark: function(p) { // Mark the points and cursor
		this.image.cleardraw();
		
		var ps, cp;
		if (p === undefined) {ps = this.pointsget(this.image.z());}
		else {ps = this.pointsget(p[2]);}
		for (var i = 0; i < ps.length; i++) {
			cp = [ps[i].x + 0.5, ps[i].y + 0.5, ps[i].z];
			if (cp[2] === p[2]) {this.image.drawcircle(cp, ps[i].r + ps[i].annulus/2, ps[i].annulus, 'rgba(204, 37, 37, 0.7)');}
			else {this.image.drawcircle(cp, ps[i].r + ps[i].annulus/2, ps[i].annulus, 'rgba(237, 207, 26, 0.7)');}
		}
		
		if (p) {
			this.pars.over = this.pointover(p);
			
			if (this.pars.over < 0) {
				cp = p.slice(0); cp[0] += 0.5; cp[1] += 0.5;
				this.image.drawcircle(cp, this.pars.r, 'rgba(106, 151, 196, 1)').drawcircle(cp, this.pars.r + this.pars.annulus, 'rgba(106, 151, 196, 0.75)').drawpixel(p, 'rgba(106, 151, 196, 0.75)').drawcircle(cp, this.pars.r + this.pars.annulus/2, this.pars.annulus, 'rgba(106, 151, 196, 0.2)');
			}
			else {
				var fp = this.pointget(this.pars.over);
				cp = [fp.x + 0.5, fp.y + 0.5, fp.z];
				this.image.drawcircle(cp, fp.r + fp.annulus/2, fp.annulus, 'rgba(166, 90, 242, 0.7)');
			}
		}
	},
	
	perspective: function(p) {
		if (p === undefined) {this.perspectiveDiv.parentNode.style.visibility = 'hidden';}
		else {
			this.perspectiveDiv.parentNode.style.visibility = 'visible';
			var nr = Math.ceil(p.r);
			this.image.perspective([p.x - nr, p.y - nr, p.z - p.spandn], [nr*2 + 1, nr*2 + 1, p.spanup + p.spandn + 1], 200, this.perspectiveCtx);
		}
		return this;
	},
	
	init: function() {
		this.pars = {
			'r': 2,
			'spanup': 2,
			'spandn': 2,
			'annulus': 1,
			'over': -1,
			'perspective': false,
			'mode': 0,
			'covermode':-1,
		};
		this.curpoly = [];
	},
	
	extend: function(o) {
		for (var key in o) {if (o.hasOwnProperty(key)) {Plasmid.prototype[key] = o[key];}}
		return this;
	},
	
//	HELPERS ===================================================================================

	bound: function(o, b) {
		return o < b[0] ? b[0] : o > b[1] ? b[1] : o;
	},
	
	commas: function(a, fix) {
		if (typeof a === "number") {
			if (fix === "undefined") {fix = 0;}
			a = a.toFixed(fix);
		}
		if (/^[-0-9.]+$/.test(a)) {
			var a = a.split('.'), trip = /(\d+)(\d{3})/;;
			var a1 = a[0], a2 = a.length > 1 ? '.' + a[1] : '';
			while (trip.test(a1)) {a1 = a1.replace(trip, '$1' + ',' + '$2');}
			return a1 + a2;
		}
		else {return a;}
	},
	
	
	attributes: function(obj, attr) {
		if (attr === undefined) {
			var out = {};
			for (var i = 0, a = obj.attributes; i < a.length; i++) {out[a.item(i).nodeName] = a.item(i).nodeValue;}
			return out;
		}
		else {
			for (var key in attr) {if (attr.hasOwnProperty(key)) {obj.setAttribute(key, attr[key]);}}
			return this;
		}
	},
	
	style: function(obj, attr) {
		if (attr === undefined) {return this.readstyle(obj);}
		else {
			var at = this.merge(this.readstyle(obj), attr);
			var sty = '';
			for (var key in at) {if (at.hasOwnProperty(key)) {sty += key + ':' + at[key] + ';';}}
			obj.setAttribute('style', sty);
			return this;
		}
	},
	
	readstyle: function(obj) {
		var out = {};
		var s = obj.getAttribute('style');
		if (s !== null) {
			s = s.split(';');
			for (var i = 0; i < s.length; i++) {
				var k = s[i].split(':');
				if (k.length === 2) {
					k[0] = k[0].trim(); k[1] = k[1].trim();
					if (k[0].length > 0 && k[1].length > 0) {out[k[0]] = k[1];}
				}
			}
		}
		return out;
	},
	
	merge: function(a, b, tonew) {
		if (tonew === undefined || tonew === true) {a = this.copy(a);}
		b = this.copy(b);
		for (var key in b) {if (b.hasOwnProperty(key)) {a[key] = b[key];}}
		return a;
	},
	
	copy: function(a) {
		var out;
		if (typeof a === "object" && a instanceof Array) {
			out = [];
			for (var i = 0; i < a.length; i++) {out.push(this.copy(a[i]));}
		}
		else if (typeof a === "object" && a !== null) {
			var out = {};
			for (var key in a) {if (a.hasOwnProperty(key)) {out[key] = this.copy(a[key]);}}
		}
		else {out = a;}
		return out;
	},
	
	ajax: function(url, data, callback) { // returns json, not good for IE 5 or 6
		function urlify(data) {
			if (data !== undefined && typeof data == "object") {
				out = [];
				for (key in data) {out.push(key + '=' + data[key].toString());}
				out.push('rand='+Math.random())
				return '?'+out.join('&');
			}
			else {return '';}
		}
		
		var req = new XMLHttpRequest();
		url += urlify(data);
		req.open("GET", url, true);
		req.onreadystatechange = function(){
			if (req.readyState == 4 && req.status == 200) {
				try {pas = JSON.parse(req.responseText);}
				catch (e) {pas = {'error':true, 'message':e.description};}
				callback(pas);
			}
		};
		req.send();
	},
	
	// Point interaction. It saves in minimal localStorage space (arrays) but expands (objects) for ease of programming.
	
	pointswitch: function(p) {
		var o;
		if (p instanceof Array) {o = {'x':p[0], 'y':p[1], 'z':p[2], 'r':p[3], 'spanup':p[4], 'spandn':p[5], 'annulus':p[6], 'sum':p[7]};}
		else {o = [p.x, p.y, p.z, p.r, p.spanup, p.spandn, p.annulus, p.sum];}
		return o;
	},
	
	pointset: function(p) {
		this.data[this.pars.file].push([p[0], p[1], p[2], this.pars.r, this.pars.spanup, this.pars.spandn, this.pars.annulus, this.image.sum(p, this.pars.r, this.pars.spanup, this.pars.spandn, this.pars.annulus)]);
		this.save();
		return this;
	},
	
	pointget: function(i, t) {
		if (t === undefined) {t = this.pars.file;}
		var d = this.data[t];
		if (i > d.length) {return false;}
		else {return this.pointswitch(d[i]);}
	},
	
	pointremove: function(i, t) {
		if (t === undefined) {t = this.pars.file;}
		this.data[t].splice(i, 1);
		this.save();
		return this;
	},
	
	pointupdate: function(n, o) {
		var d = this.pointswitch(this.data[this.pars.file][n]);
		for (var key in o) {if (o.hasOwnProperty(key)) {d[key] = o[key];}}
		d.sum = this.image.sum([d.x, d.y, d.z], d.r, d.spanup, d.spandn, d.annulus);
		this.data[this.pars.file][n] = this.pointswitch(d);
		this.save();
		return this;
	},
	
	pointsget: function(z) {
		var d = this.data[this.pars.file];
		var o = [];
		
		if (z !== undefined) {
			for (var i = 0; i < d.length; i++) {
				if (d[i][2] === z) {o.push(this.pointswitch(d[i]));}
				else if (z >= d[i][2] - d[i][5] && z <= d[i][2] + d[i][4]) {o.push(this.pointswitch(d[i]));}
			}
		}
		else {
			for (var i = 0; i < d.length; i++) {o.push(this.pointswitch(d[i]));}
		}
		
		return o;
	},
	
	allpoints: function(t) {
		if (t === undefined) {t = this.pars.file;}
		var d = this.data[t];
		var o = [];
		for (var i = 0; i < d.length; i++) {o.push(this.pointswitch(d[i]));}
		return o;
	},
	
	pointsum: function(t) {
		if (t === undefined) {t = this.pars.file;}
		var d = this.data[t];
		var sum = 0;
		for (var i = 0; i < d.length; i++) {sum += d[i][7];}
		return sum;
	},
	
	pointover: function(p) {
		var o = -1;
		var d = this.data[this.pars.file];	
		
		for (var i = 0; i < d.length; i++) {
			if (d[i][2] === p[2]) {
				if ((p[0] - d[i][0])*(p[0] - d[i][0]) + (p[1] - d[i][1])*(p[1] - d[i][1]) < (d[i][3] + d[i][6])*(d[i][3] + d[i][6])) {o = i;}
			}
			else if (p[2] >= d[i][2] - d[i][5] && p[2] <= d[i][2] + d[i][4]) {
				if ((p[0] - d[i][0])*(p[0] - d[i][0]) + (p[1] - d[i][1])*(p[1] - d[i][1]) < (d[i][3] + d[i][6])*(d[i][3] + d[i][6])) {o = i;}
			}
		}
				
		return o;
	},
	
	noteset: function() {
		this.notes[this.pars.file] = this.pars.notediv.value;
		this.save();
	},
};