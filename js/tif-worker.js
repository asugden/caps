/* jshint sub: true */
// Add in fingerprinting
// Add in ability to read color TIFFs
// Add in ARTfile interleaving fix

"use strict";
function ARTfile(){} function artfile(fl) { // Takes file
	var out = new ARTfile();
	out.init(fl);
	return out;
}

ARTfile.prototype = {
	tell: function() {return this.pars.chunkoffset + this.p;},
	seek: function(loc) {
		if (loc < this.pars.chunkmax - 1 && loc > this.pars.chunkoffset) {this.p = loc - this.pars.chunkoffset;}
		else {this.open(loc);}
		return this;
	},
	
	location: function(off) { // Moves to a new location in the file
		if (off === undefined) {return this.tell();}
		else {this.seek(off);}
		return this;
	},
	
	int: function(sz, u) { // Returns an int of size sz with the optional unsigned value
		if (this.p + sz >= this.pars.chunksize) {this.open();}
		var ts = {
			'i1':'Int8',
			'i2':'Int16',
			'i4':'Int32',
			'u1':'Uint8',
			'u2':'Uint16',
			'u4':'Uint32',
		};
		var o = (u === undefined || u === false) ? this.bin['get'+ts['i'+sz]](this.p, this._littleEndian) : this.bin['get'+ts['u'+sz]](this.p, this._littleEndian);
		this.p += sz;
		return o;
	},
	
	float: function(sz) { // return a float of size sz
		if (this.p + sz >= this.pars.chunksize) {this.open();}
		var o = this.bin['getFloat'+sz*8](this.p, this._littleEndian);
		this.p += sz;
		return o;
	},
	
	string: function(len) { // return a string of size len
		if (len === undefined) {len = 1;}
		
		var lens = [len], value = '';
		if (this.p + len >= this.pars.chunksize) {
			var next = this.pars.chunksize - this.p;
			lens[0] = next;
			len -= next;
			while (len > 0) {
				next = Math.min(len, this.pars.chunksize);
				lens.push(next);
				len -= next;
			}
			
		}
		
		for (var j = 0; j < lens.length; j++) {
			if (j > 0) {this.open();}
			for (var i = 0; i < lens[j]; i++) {
				var chr = this.bin.getUint8(this.p + i, this._littleEndian);
				value += String.fromCharCode(chr > 127 ? 65533 : chr);
			}
			this.p += lens[j];
		}
		
		return value.replace(/\0/g, '');
	},
	
	hex: function(len) { // return a hex of size len
		if (len === undefined) {len = 1;}
		
		var lens = [len], value = '';
		if (this.p + len >= this.pars.chunksize) {
			var next = this.pars.chunksize - this.p;
			lens[0] = next;
			len -= next;
			while (len > 0) {
				next = Math.min(len, this.pars.chunksize);
				lens.push(next);
				len -= next;
			}
		}
		
		for (var j = 0; j < lens.length; j++) {
			if (j > 0) {this.open();}
			for (var i = 0; i < lens[j]; i++) {
				value += this.bin.getUint8(this.p + i, this._littleEndian).toString(16);
			}
			this.p += lens[j];
		}
		
		return value;
	},
	
	image: function(bits, positionarray, sizearray, fullsize, channels, interleaved) {
		if (interleaved === undefined) {interleaved = false;}
		var out, blob;
		
		if (positionarray.length === 1) {
			blob = this.file.slice(positionarray[0], positionarray[0] + sizearray[0]);
			
			if (bits <= 8) {out = new Uint8Array(this.reader.readAsArrayBuffer(blob));}
			else if (bits <= 16) {out = new Uint16Array(this.reader.readAsArrayBuffer(blob));}
			else if (bits <= 32) {out = new Uint32Array(this.reader.readAsArrayBuffer(blob));}
		}
		else {
			var i, ln, offset = 0;
			if (bits <= 8) {
				out = new Uint8Array(fullsize);
				for (i = 0; i < positionarray.length; i++) {
					blob = this.file.slice(positionarray[i], positionarray[i] + sizearray[i]);
					ln = new Uint8Array(this.reader.readAsArrayBuffer(blob));
					out.set(ln, offset);
					offset += sizearray[i];
				}
			}
			else if (bits <= 16) {
				out = new Uint16Array(fullsize);
				for (i = 0; i < positionarray.length; i++) {
					blob = this.file.slice(positionarray[i], positionarray[i] + sizearray[i]);
					ln = new Uint16Array(this.reader.readAsArrayBuffer(blob));
					out.set(ln, offset);
					offset += sizearray[i]/2;
				}
			}
			else if (bits <= 32) {
				out = new Uint32Array(fullsize);
				for (i = 0; i < positionarray.length; i++) {
					blob = this.file.slice(positionarray[i], positionarray[i] + sizearray[i]);
					ln = new Uint32Array(this.reader.readAsArrayBuffer(blob));
					out.set(ln, offset);
					offset += sizearray[i]/4;
				}
			}
		}
		
		// Add in interleaving fixing here
		
		return out;
		
	},
	
	advance: function(n) { // advances the binary
		this.seek(this.p + n);
		return this;
	},
	
	littleendian: function(tf) {
		this._littleEndian = tf;
		return this;
	},
	
	bigendian: function(tf) {
		this._littleEndian = tf === true ? false : true;
		return this;
	},
	
	endian: function(littleorbig) {
		this._littleEndian = littleorbig === 'little' ? true : false;
		return this;
	},
	
	// =======================================================
	
	init: function(fl) {
		this.reader = new FileReaderSync();
		this.file = fl;
		this.pars.filesize = fl.size;
		
		return this;
	},
	
	open: function(start, stop) {
		if (start === undefined) {start = this.pars.chunkoffset + this.p;}
		if (stop === undefined) {stop = Math.min(start + this.pars.chunkdefault, this.pars.filesize);}
		else if (stop > this.pars.filesize - 1) {stop = this.pars.filesize - 1;}
		
		this.pars.chunkoffset = start;
		this.pars.chunkmax = stop;
		this.pars.chunksize = stop - start;
		this.p = 0;
		
		this.reader = new FileReaderSync();
		var blob = this.file.slice(start, stop + 1);
		this.bin = new DataView(this.reader.readAsArrayBuffer(blob));
	},
	
	pars: {
		filesize: 0,
		chunkoffset: 0,
		chunksize: -1,
		chunkmax: -1,
		chunkdefault: 1e6, // 1e6 is a good chunk size for 
	},
	
	_littleEndian: true,
};





function TIF(){} function tif(fl, chunksize) {
	var out = new TIF();
	out.init(fl, chunksize);
	return out;
}

TIF.prototype = {
	init: function(fl, chunksize) {
		this.bin = artfile(fl);
		if (chunksize) {this.bin.chunksize = chunksize;}
		this.tags = [];
		this.pars = {};
	},
	
	header: function(prev) {
		if (prev === undefined) {
			this.bin.seek(0);
			this.ifd = 0;
			this.tags.push([]);
			
			var lend = this.bin.hex(2) === '4949' ? true : false;
			this.bin.littleendian(lend);
			if (this.bin.int(2, true) != 42) {throw "Non-standard file";}
			
			this.bin.seek(this.bin.int(4, true));
			var count = this.bin.int(2);
			for (var i = 0; i < count; i++) {this.getTag();}
			this.nextifd = this.bin.int(4, true);
		}
		else {this.pars = prev;}
		
		return this;
	},
	
	compute: function() {
		this.zs();
		this.id();
		this.setpars();
		return this;
	},
	
	zs: function() {
		while (this.nextifd > 0) {
			this.ifd++;
			this.bin.seek(this.nextifd);
			this.tags.push([]);
			var count = this.bin.int(2);
			for (var i = 0; i < count; i++) {this.getTag();}
			this.nextifd = this.bin.int(4, true);
		}
		
		return this;
	},
	
	setpars: function() {
		if (!this.pars.width) {
			this.pars = {
				'id':this.pars.id,
				'depth':this.tags.length,
				'width':this.tags[0].ImageWidth,
				'height':this.tags[0].ImageLength,
				'channels':1, // Fix this for reading color TIFFs
				'bits':this.tags[0].BitsPerSample,
				'zs': [],
				'zsizes': [],
			};
			
			for (var i = 0; i < this.pars.depth; i++) {
				this.pars.zs.push(this.tags[i]['StripOffsets']);
				this.pars.zsizes.push(this.tags[i]['StripByteCounts']);
			}
		}
	},
	
	id: function() {
		if (this.pars.id === 0) {
			var start, end;
			if (this.tags[0]['DateTime']) {
				this.pars.id = this.tag(this.tags[0]['DateTime']);
			}
			else if (this.tags[0]['Software'] && this.tags[0]['Software'].trim() == 'OME Bio-Formats') {
				start = this.tags[0]['ImageDescription'].indexOf('UUID="') + 6;
				end = this.tags[0]['ImageDescription'].indexOf('"', start + 1);
				if (start > -1 && end > -1) {
					var uuid = this.tags[0]['ImageDescription'].substring(start, end);
					this.pars.id = uuid.substring(uuid.lastIndexOf(':') + 1);
				}
			}
			else if (this.tags[0]['ImageJ']) {
				start = this.tags[0]['ImageJ'].indexOf('ImageJ File Date');
				start = this.tags[0]['ImageJ'].indexOf(start, '=') + 1;
				end = this.tags[0]['ImageJ'].indexOf(start, '\n');
				this.pars.id = parseInt(this.tags[0]['ImageJ'].substring(start, end)).toString();
			}
			// Add in fingerprinting
		}
		
		return this.pars.id;
	},
	
	parameters: function() {return this.pars;},
	
	tagnames: {
		254:'NewSubfileType',
		256:'ImageWidth',
		257:'ImageLength',
		258:'BitsPerSample',
		259:'Compression',
		262:'PhotometricInterpretation',
		270:'ImageDescription',
		273:'StripOffsets',
		277:'SamplesPerPixel',
		278:'RowsPerStrip',
		279:'StripByteCounts',
		282:'XResolution',
		283:'YResolution',
		284:'PlanarConfiguration',
		296:'ResolutionUnit',
		305:'Software',
		306:'DateTime',
		320:'ColorMap',
		339:'SampleFormat',
		50839:'ImageJ',
	},
	
	/* Unused-- split into switch statements to speed up reading of tags
	types: {
		// Types of tags
		1:{'name':'byte', size:1, 'fn':function(s){return s.bin.int(1, true);}},
		2:{'name':'ascii', 'size':1, 'fn':function(s){return s.bin.string(1);}},
		3:{'name':'short', 'size':2, 'fn':function(s){return s.bin.int(2, true);}},
		4:{'name':'long', 'size':4, 'fn':function(s){return s.bin.int(4, true);}},
		5:{'name':'rational', 'size':8, 'fn':function(s){return s.bin.int(4, true)/s.bin.int(4, true);}},
	},*/
	
	typesizes: [1,1,1,2,4,8],
	
	getTag: function() {
		// Returns all of the tags and skips tags specific to different companies. Should be modified to understand ImageJ specific tags.
		var start = this.bin.tell(), tag = this.bin.int(2, true), type = this.bin.int(2, true), count = this.bin.int(4, true);
		var i, out;
		
		if (tag === 50839) {type = 2;}		
		tag = this.tagnames[tag] !== undefined ? this.tagnames[tag] : tag;
		
		if (this.typesizes[type]*count > 4) {
			out = {'type':type, 'count':count, 'pos':this.bin.int(4, true)};
		}
		else {
			if (count > 1) {
				switch(type) {
					case 1:
						out = [];
						for (i = 0; i < count; i++) {out.push(this.bin.int(1, true));}
						break;
					case 2:
						out = this.bin.string(count - 1);
						break;
					case 3:
						out = [];
						for (i = 0; i < count; i++) {out.push(this.bin.int(2, true));}
						break;
				}
			}
			else {
				switch(type) {
					case 1:
						out = this.bin.int(1, true);
						break;
					case 2:
						out = this.bin.string(1);
						break;
					case 3:
						out = this.bin.int(2, true);
						break;
					case 4:
						out = this.bin.int(4, true);
						break;
				}
			}
		}
		
		this.tags[this.ifd][tag] = out;
		this.bin.seek(start + 12);
		return this;
	},
	
	tag: function(tg) {
		if (typeof tg === 'object') {
			var i, out;
			this.bin.seek(tg.pos);
			
			switch(tg.type) {
				case 1:
					out = [];
					for (i = 0; i < tg.count; i++) {out.push(this.bin.int(1, true));}
					break;
				case 2:
					out = this.bin.string(tg.count - 1);
					break;
				case 3:
					out = [];
					for (i = 0; i < tg.count; i++) {out.push(this.bin.int(2, true));}
					break;
				case 4:
					out = [];
					for (i = 0; i < tg.count; i++) {out.push(this.bin.int(4, true));}
					break;
				case 5:
					if (tg.count > 1) {
						out = [];
						for (i = 0; i < tg.count; i++) {out.push(this.bin.int(4, true)/this.bin.int(4, true));}
					}
					else {out = this.bin.int(4, true)/this.bin.int(4, true);}
					break;
			}
			
			return out;
		}
		else {return tg;}
	},
	
	z: function(z) {
		if (this.pars.zs[z].length === undefined) {
			this.pars.zs[z] = this.tag(this.pars.zs[z]);
			this.pars.zsizes[z] = this.tag(this.pars.zsizes[z]);
			
			var newzs = [this.pars.zs[z][0]], newzsizes = [], newsize = this.pars.zsizes[z][0], lastz = this.pars.zs[z][0], lastzsize = this.pars.zsizes[z][0];
			for (var i = 1; i < this.pars.zs[z].length; i++) {
				if (this.pars.zs[z][i] === lastz + lastzsize) {
					lastzsize = this.pars.zsizes[z][i];
					newsize += lastzsize;
				}
				else {
					newzsizes.push(newsize);
					newsize = 0;
					newzs.push(this.pars.zs[z][i]);
				}
				
				lastz = this.pars.zs[z][i];
			}
			
			newzsizes.push(newsize);
			
			this.pars.zs[z] = newzs;
			this.pars.zsizes[z] = newzsizes;
		}
		
		return this.bin.image(this.pars.bits, this.pars.zs[z], this.pars.zsizes[z], this.pars.width*this.pars.height, this.pars.channels);
	},
};


self.addEventListener('message', function(e) {
	var data = e.data;
	if (data.file) {
		this.fio = tif(data.file);
		this.fio.header(data.header);
	}
		
	if (data.id) {
		self.postMessage(this.fio.id(), data.identifier);
	}
	else {
		this.fio.compute();
		
		if (data.parameters) {
			self.postMessage({'parameters':this.fio.parameters()});
		}
		if (data.z) {
			var im = this.fio.z(data.z);
			self.postMessage({'z':data.z, 'pars':this.fio.parameters(), 'im':im}, [im.buffer]);
		}
	}
		
}, false);
