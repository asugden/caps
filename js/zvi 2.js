// Created by Arthur Sugden in August 2011
// Reads files created by Axiovision by Zeiss
// File format copyright Zeiss
// copyright 2011 Arthur Sugden

function ZVI(){}
ZVI.prototype = {
	// ARTIM- input output functions
	read: function(data) {
		this.bin = new jDataView(data);
		
		// Any Microsoft Compound Format File
		try {this.getHeader().getMasterSectors().getSectorAllocationTable();}
		catch (e) {console.log("ERROR: Header data: " + e); return false;}
		this.getDirectory();
		
		// Particular to ZVI format
		this.getImagePars();
		
		return this;
	},
	
	z: function(set) {this.getZLevel(set); return this;}, // sets the z level
	zs: function() {return this.pars.depth;}, // returns number of z levels
	bits: function() {return this.pars.validBits;}, // returns the bit-depth
	async: function() {return true;}, // checks if the filetype can be read asynchronously
	width: function() {return this.pars.width;},
	height: function() {return this.pars.height;},
	id: function() {return this.directory[0].time;},
	data: function() {return this.d;}, // returns the image data-- this will be updated as it is loaded
	


	// ===========================================================================
	// COMMON- OLE compound file functions
	getHeader: function() { 
		// Returns the general OLE compound file format header
		// This always begins at position 0 and gives information necessary for the rest of the document retrieval. Some positions are skipped with the location command because they are unused. I believe that all of the integers are signed.
		this.location(0);
		if (this.location(0).hex(8) != 'd0cf11e0a1b11ae1') {throw "Non-standard file"; return false;}
		
		this.pars = {
			'littleEndian': this.location(28).hex(2) == 'feff' ? true : false,
			'sectorSize': Math.pow(2, this.int(2)),
			'shortSectorSize': Math.pow(2, this.int(2)),
			'numSectors': this.location(44).int(4),
			'directoryID': this.int(4),
			'minimumStreamSize': this.location(56).int(4),
			'firstShortID': this.int(4),
			'numShortSectors': this.int(4),
			'firstMasterID': this.int(4),
			'numMasterSectors': this.int(4),
		};
		return this;
	},

	getMasterSectors: function() {
		// Returns the sectors of the master table from which the sector allocation table is built
		// The first 109 sector positions are saved in the header. -2 is the special ending character of the master allocation table
		this.masters = [];
		this.location(76);
		for (var i = 0; i < 109; i++) {
			var val = this.int(4);
			if (val > -1) {this.masters.push(val);}
		}
		
		if (this.pars.numMasterSectors > 109) {
			var id = this.pars.firstMasterID, end = false;
			while (!end) {
				this.stream(id);
				for (var i = 0; i < this.pars.sectorSize/4; i++) {
					var id = this.int(4);
					if (id > 0) {this.masters.push(id);}
					else if (id == -2) {end = true;}
				}
			}
		}
		
		return this;
	},
	
	getSectorAllocationTable: function() {
		// Returns the main sector allocation table (or FAT)
		// The positions of the SAT are stored in the master sectors, determined above
		var sl = this.pars.sectorSize/4;
		this.sat = new Array(this.masters.length*(this.pars.sectorSize/4));
		for (var i = 0; i < this.masters.length; i++) {
			this.location((this.masters[i] + 1)*this.pars.sectorSize);
			for (var j = 0; j < sl; j++) {this.sat[i*sl + j] = this.int(4);}
		}
		return this;
	},
	
	directoryListing: function(id) {
		// Returns a directory object for a directory id
		var l = this.stream(this.pars.directoryID, id*128 + 64).int(2) - 2;				
		this.stream(this.pars.directoryID, id*128);
		console.log(l);
		var types = {'0':'empty', '1':'user storage', '2':'user stream', '3':'lockbytes', '4':'property', '5':'root storage'};		
		var out = {
			'name':this.string(l),
			'type':types[this.skip(66 - l).hex()],
			'color':this.hex() == '0' ? 'red' : 'black',
			'leftChild':this.int(4),
			'rightChild':this.int(4),
			'root':this.int(4),
			'time':this.skip(20).timestamp(), // skip unused bytes
			'id':this.skip(8).int(4), // skip modified timestamp
			'bytes':this.int(4),
		};
		return out;
	},
	
	getDirectory: function() {
		// Get red-black directory listing
		var self = this;
		
		function sideways(val) {
			var listing = self.directoryListing(val);
			var out = [{'name':listing.name, 'id':listing.id, 'type':listing.type, 'bytes':listing.bytes, 'sub':listing.root, 'time':listing.time}];
			console.log(out, listing);
			if (listing.leftChild != -1) {
				var resp = sideways(listing.leftChild);
				for (var i = 0; i < resp.length; i++) {out.push(resp[i]);}
			}
			if (listing.rightChild != -1) {
				var resp = sideways(listing.rightChild);
				for (var i = 0; i < resp.length; i++) {out.push(resp[i]);}
			}
			
			return out;
		}
		
		function traverse(val) {
			var out = sideways(val);
			for (var i = 0; i < out.length; i++) {
				if (out[i].sub != -1) {
					out[i].sub = traverse(out[i].sub);
				}
			}
			return out;
		}
		console.log('before', this.directoryListing(0));
		console.log(sideways(1));
		var dir = traverse(0)[0];
		console.log('after');

//		Particular to ZVI		
		var Image = -1;
		var i = 0;
		while (Image < 0 && i < dir.sub.length) {
			if (dir.sub[i].name == 'Image') {Image = i;}
			i++;
		}
		if (Image == -1) {alert('Corrupted File (Image)'); return 0;}
		
		var out = new Array(dir.sub[Image].sub.length - 5);
		for (i = 0; i < dir.sub[Image].sub.length; i++) {
			if (dir.sub[Image].sub[i].name.indexOf('Item') > -1) {
				out[parseInt(dir.sub[Image].sub[i].name.substr(4))] = dir.sub[Image].sub[i];
			}
		}
		
		if (out[out.length - 1] === undefined) {out.splice(out.length - 1, 1);}
		this.directory = out;
		this.zviDirectory = dir;
		
		return this;
	},
	
	
	
	// ===========================================================================
	// ZVI SPECIFIC- Functions for reading ZVI files
	getImagePars: function() { // Get the image parameters from the first image
		var o = {
			'depth':this.directory.length,
			'width':this.stream(this.directory[0].sub[0].id, 16).token(),
			'height':this.token(),
			'pixelType':this.token(1).token(),
			'validBits':this.token(),
		}
		for (key in o) {this.pars[key] = o[key];}
		this.d = new Array(this.pars.depth);
		return this;
	},
		
	getZLevel: function(z) {
		this.stream(this.directory[z].sub[0].id).token(16); // version, type, typedescription, and filename– not used
//		console.log(this.bin.tell());
//		this.bin.seek(this.bin.tell() + 20);
		
		// THIS PART IS UGLY
		var i = 0;
		var last = '';
		var found = 0;
		while (i < 2000 && found == 0) {
			var current = this.bin.getUint8();
			if (current == 32) {
				if (last == 0) {
					if (this.bin.getUint8() == 0) {
						if (this.bin.getUint8() == 16) {
							found = 1;
						}
						this.bin.seek(this.bin.tell() - 1);
					}
					this.bin.seek(this.bin.tell() - 1);
				}
			}
			last = current;
			i++;
		}
		if (found == 0) {console.log('ERROR: Problem with file (version search)'); return 0;}
		
		this.bin.seek(this.bin.tell() + 26);
//		console.log('start of thing', z, this.bin.tell());
		out = new Array(this.pars.height);
		for (var y = 0; y < this.pars.height; y++) {
			out[y] = new Array(this.pars.width);
			for (var x = 0; x < this.pars.width; x++) {
				out[y][x] = this.bin.getUint16();
			}
		}
		
		this.d[z] = out;
		
		return this;
	},
	
	
	// Functions for internal moving around ole document
	stream: function(id, off) { // sets the active stream with the id beginning the stream and the optional offset
		if (off === undefined) {off = 0;}
		else if (off > this.pars.sectorSize) {
			var noff = Math.floor(off/this.pars.sectorSize);
			for (var i = 0; i < noff; i++) {id = this.sat[id];}
			if (id < 0) {return false;}
			off -= noff*this.pars.sectorSize;
		}
		this.active = {'id': id,'off': off};
		this.bin.seek((id + 1)*this.pars.sectorSize + off);
		return this;
	},
	
	location: function(off) { // turns off active stream, sets offset from the start of the file
		this.active = {'id':false, 'off':off};
		this.bin.seek(off);
		return this;
	},
	
	shortstream: function(id, off) { // sets the active stream to the short stream
		if (off === undefined) {off = 0;}
		this.active = {'id': this.pars.firstShortID, 'off': id*this.pars.shortSectorSize + off};
		this.bin.seek(this.streambyid(this.active.id, this.active.off));
		return this;
	},
	
	token: function(n) {// an int16, data pair where int16 describes the following data type
		var self = this;
		function get() {
			var type = self.int(2);
			switch (type) {
				case 2: // 16-bit integer
					return self.int(2);
					break;
				case 3: // 32-bit integer
					return self.int(4);
					break;
				case 4: // 32-bit float
					return self.float(4);
					break;
				case 5: // 64-bit float
					return self.getFloat(8);
					break;
				case 7: // 64-bit float
					return self.float(8);
					break;
				case 8: // VT_BSTR streamed storage
					var l = self.int(2);
					return self.string(l);
					break;
				case 11: // VT_BOOL 32-bit integer, true of !0
					var o = self.int(4);
					return o == 0 ? false : true;
					break;
				case 16: // 8-bit integer
					return self.int(1);
					break;
				case 17: // 8-bit unsigned integer
					return self.int(1, true);
					break;
				case 18: // 16-bit unsigned integer
					return self.int(2, true);
					break;
				case 19: // 32-bit unsigned integer
					return self.int(4, true);
					break;
				case 20: // 64-bit integer– not allowed in jdataview
				
					break;
				case 21: // 64-bit unsigned integer– not allowed in jdataview
					
					break;
				case 65: // VT_BLOB binary data
					var l = self.int(4);
					return self.string(l);
					break;
				case 68: // VT_STORED_OBJECT streamed storage
					var l = self.int(2);
					return self.string(l);
					break;
				
				return false;
			}
		}
		if (n === undefined) {return get();}
		else {
			for (var i = 0; i < n; i++) {get();}
			return this;
		}
	},
	
	int: function(sz, u) { // returns an int of size sz with the optional unsigned value and advances the position
		var ts = {
			'i1':'Int8',
			'i2':'Int16',
			'i4':'Int32',
			'u1':'Uint8',
			'u2':'Uint16',
			'u4':'Uint32',
		};
		var o = (u === undefined || u === false) ? this.bin['get'+ts['i'+sz]]() : this.bin['get'+ts['u'+sz]]();
		this.advance(sz);
		return o;
	},
	
	float: function(sz) { // return a float of size sz and advances the position in the binary
		var o = this.bin['getFloat'+sz*8]();
		this.advance(sz);
		return o;
	},
	
	string: function(len) { // return a string of size len and advances the position in the binary
		if (this.active.off + len < this.pars.sectorSize) {
			var o = this.bin.getString(len);
			this.advance(len);
			return o.replace(/(?=\D)\W/ig, '');
		}
		else {
			var s = '';
			for (var i = 0; i < len; i++) {
				s += this.bin.getChar();
				this.advance(1);
			}
			return s.replace(/(?=\D)\W/ig, '');
		}
	},
	
	hex: function(l) { // return a hex of size len and advances the position in the binary
		if (l === undefined) {l = 1;}
		var o = '';
		for (var i = 0; i < l; i++) {o += this.int(1, true).toString(16);}
		return o;
	},
	
	timestamp: function() { // gets a timestamp and converts it to javascript time. Stupidly, it converts from hex to decimal, but that's because there is no getInt64
		function parseLittleEndianHex(x) {
			var o = 0;
			for (var p = 0; p < x.length/2; p++) {o += parseInt(x.substring(p*2, p*2 + 2), 16)*Math.pow(2, p*8)/10000;}
			return o;
		}
		
		function reverseHex(x) {
			var o = '';
			for (var p = x.length; p > 0 ; p -= 2) {o += x.substring(p - 2, p);}
			return o;
		}
		
		var a = (this.hex(8));
		if (a != 0) {return reverseHex(a);}
		else {return false;}
	},
	
	skip: function(n) { // skips n bytes
		this.active.off += n;
		this.bin.seek(this.streambyid(this.active.id, this.active.off));
		return this;
	},
	
	advance: function(n) { // advances the binary within a stream -- ONLY INTERNAL TO TYPE METHODS
		this.active.off += n;
		if (this.active.id && this.active.off >= this.pars.sectorSize) {
			this.active.id = this.sat[this.active.id];
			this.active.off -= this.pars.sectorSize;
			this.bin.seek(this.streambyid(this.active.id, this.active.off));
		}
		return this;
	},
	
	// Function for getting sector and stream positions-- could be fixed by a better programmer
	streambyid: function(id, off) {
		var noff = Math.floor(off/this.pars.sectorSize);
		for (var i = 0; i < noff; i++) {id = this.sat[id];}
		if (id < 0) {return false;}
		else {return (id + 1)*this.pars.sectorSize + (off - noff*this.pars.sectorSize);}
	},
	
};